//
//  LoginRepository.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/3/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class LoginRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter

    public init(_ client: APIRouter = NetworkClient()) {
        networkClient = client
        cacher = Cacher(destination: .atFolder("Login"))
    }
    func login(with userName: String, and password: String, completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.login().url else { return }
        let paramters = ["Email": userName, "Password": password]
        let request = makeRequest(url: url, parameters: paramters, type: .post)
        getData(withRequest: request,
        name: String(describing: LoginResultData.self),
        decodingType: MagixResponse<LoginResultData>.self,
        completion: completion)
    }
    func sendRegisterationToken(with paramters: [String: String],
                                completion: @escaping
        RepositoryCompletion) {
        guard let url = Endpoint.sendToken().url else { return }
        let request = makeRequest(url: url, parameters: paramters, type: .put)
        getData(withRequest: request,
        name: String(describing: RegiserationTokenMode.self),
        decodingType: MagixResponse<RegiserationTokenMode>.self,
        completion: completion)
    }
    func getApplicationSettings(completion: @escaping
        RepositoryCompletion) {
        guard let url = Endpoint.applicationSettings().url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: ApplicationSettings.self),
                decodingType: MagixResponse<ApplicationSettings>.self,
                completion: completion)
    }
}
