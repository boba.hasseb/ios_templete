//
//  ApplicationSettings.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/19/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
public struct ApplicationSettings: Codable {
    let leafletURL: String?
    let enableNotification: Bool?
}
