//
//  LoginModel.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/3/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct LoginResultData: Codable {
    var userID: String?
    var firstName: String?
    var lastName: String?
    var type: String?
    var imagePath: String?
    var qrImagePath: String?
    var email: String?
    var bio: String?
    var jobTitleStr: String?
    var mobilePhone: String?
    var appContent: AppContentUser?
}

struct AppContentUser: Codable {
    var contentID: String?
}
public enum UserType: String {
    case student
    case teacher
}
