//
//  RegiserationTokenModel.swift
//  MagixCampus
//
//  Created by User on 4/2/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct RegiserationTokenMode: Codable {
    var userID: String
    var applicationID: String
    var deviceType: String
    var reg: String
}
