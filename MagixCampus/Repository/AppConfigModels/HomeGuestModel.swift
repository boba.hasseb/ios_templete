//
//  HomeGuestModel.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/2/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct HomeGuestModel: Codable {
    let chattingEnabled: Bool
}
