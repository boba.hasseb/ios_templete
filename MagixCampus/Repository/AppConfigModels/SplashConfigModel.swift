//
//  ConfigModel.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/2/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct SplashConfig: Codable {
    let authorizedDirection: String
    let notAuthorizedDirection: String
}
