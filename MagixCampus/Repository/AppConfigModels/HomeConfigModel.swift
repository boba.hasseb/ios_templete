//
//  HomeConfigModel.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct HomeConfigModel: Codable {
    let chattingEnabled: Bool
    let campusMapEnabled: Bool
    let campusNewsEnabled: Bool
    let campusInformationEnabled: Bool
}
