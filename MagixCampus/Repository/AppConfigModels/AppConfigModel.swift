//
//  ConfigModel.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct AppConfigModel: Codable, Cachable {
    var fileName: String? = String(describing: AppConfigModel.self)
    let result: Config
}
struct Config: Codable {
    let splash: SplashConfig
    let login: LoginConfig
    let home: HomeConfigModel
    let directoryDetails: DirectoryDetailsConfigModel
    let homeGuest: HomeGuestModel
}
