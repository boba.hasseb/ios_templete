//
//  LoginModel.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/2/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct LoginConfig: Codable {
    let forgetPasswordActionEnabled: Bool
}
