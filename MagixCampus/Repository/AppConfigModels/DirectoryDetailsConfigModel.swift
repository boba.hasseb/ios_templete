//
//  DirectoryDetailsConfigModel.swift
//  MagixCampus
//
//  Created by User on 3/31/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct DirectoryDetailsConfigModel: Codable {
    let chattingEnabled: Bool
    let videoCallEnabled: Bool
}
