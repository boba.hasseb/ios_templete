//
//  SplashRepository.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class SplashRepository: Repository {

    let cacher: Cacher
    let networkClient: APIRouter

    public init(_ client: APIRouter = LocalClient(fileName: "app_configuration")) {
        networkClient = client
        cacher = Cacher(destination: .atFolder("Splash"))
    }

    func getConfig(completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.survey().url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
        name: String(describing: AppConfigModel.self),
        decodingType: AppConfigModel.self, completion: completion)
    }
}
