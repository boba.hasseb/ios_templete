//
//  NETWORK+ENDPOINTS.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 2/10/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

extension Endpoint {
    static func survey() -> Endpoint {
        let path =
        "/api/v1/organization/\(Environment.organizationID)/application/\(Environment.appID)/survey"
        return Endpoint(base: Environment.accountURL,
                        path: path)
    }
    static func login() -> Endpoint {
        let path =
        "/api/v2/organization/\(Environment.organizationID)/application/\(Environment.appID)/user/login"
        return Endpoint(base: Environment.accountURL,
                        path: path)
    }
    static func signUp() -> Endpoint {
        let path =
        "/api/v1/organization/\(Environment.organizationID)/application/\(Environment.appID)/user/register"
        return Endpoint(base: Environment.registrationURL,
                        path: path)
    }
    static func list() -> Endpoint {
        let path =
        "/api/v1/organization/\(Environment.organizationID)/application/\(Environment.appID)/hotelRecommendationList"
        return Endpoint(base: Environment.accountURL,
                        path: path)
    }
    static func categoryContent(identifier: String) -> Endpoint {
        let path =
            "/api/v2/organization/" +
        "\(Environment.organizationID)/application/\(Environment.appID)/appcontentcategory/\(identifier)/appcontent"
        return Endpoint(base: Environment.accountURL,
                        path: path)
    }
    static func sendToken() -> Endpoint {
        let base = "/api/v1/organization/\(Environment.organizationID)/application/\(Environment.appID)"
        let path =
        "\(base)/user/\(UserDefaults.standard.userID ?? "")/device/registration"
        return Endpoint(base: Environment.accountURL,
                        path: path)
    }
    static func notificationList() -> Endpoint {
       let path = "/api/v2/organization/\(Environment.organizationID)/application/"
               + "\(Environment.appID)/notification"

                return Endpoint(base: Environment.baseURL,

                                path: path)
    }
    static func newsList() -> Endpoint {
        let path = "/api/v2/organization/\(Environment.organizationID)/application/"
        + "\(Environment.appID)/appcontenttype/news/content"

         return Endpoint(base: Environment.baseURL,

                         path: path)
     }
    static func programList() -> Endpoint {
        let path =
"/api/v2/organization/\(Environment.organizationID)/application/\(Environment.appID)/appcontenttype/programs/content"
        return Endpoint(base: Environment.baseURL,
                        path: path)
    }
    static func newsDetails(contentId: String) -> Endpoint {
        let path = "/api/v2/organization/\(Environment.organizationID)/application/"
        + "\(Environment.appID)/appcontenttype/news/content/\(contentId)"
         return Endpoint(base: Environment.baseURL,
                         path: path)
     }
    static func coursesList() -> Endpoint {
        let path = "/api/v2/organization/\(Environment.organizationID)/application/"
        + "\(Environment.appID)/appcontenttype/courses/content"

         return Endpoint(base: Environment.baseURL,

                         path: path)
     }
    static func informationList() -> Endpoint {
        let path = "/api/v2/organization/\(Environment.organizationID)/application/"
        + "\(Environment.appID)/appcontenttype/information/content"

         return Endpoint(base: Environment.baseURL,

                         path: path)
     }
    static func informationDetails(contentId: String) -> Endpoint {
        let path = "/api/v2/organization/\(Environment.organizationID)/application/"
        + "\(Environment.appID)/appcontenttype/information/content/\(contentId)"
         return Endpoint(base: Environment.baseURL,
                         path: path)
     }
    static func directoryList(userType: String) -> Endpoint {
        let path = "/api/v2/organization/\(Environment.organizationID)/application/" +
            "\(Environment.appID)/usertype/\(userType)/user"
        return Endpoint(base: Environment.accountURL,
                        path: path)
     }
    static func directoryDetails(userType: String, userId: String) -> Endpoint {
        let path = "/api/v2/organization/\(Environment.organizationID)/application/" +
            "\(Environment.appID)/usertype/\(userType)/user/\(userId)"
        return Endpoint(base: Environment.accountURL,
                        path: path)
     }
    static func applicationSettings() -> Endpoint {
        let path = "/api/v2/organization/\(Environment.organizationID)/application/"
            + "\(Environment.appID)"
        return Endpoint(base: Environment.baseURL,
                        path: path)
    }
}
