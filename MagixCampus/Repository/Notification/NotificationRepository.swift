//
//  NotificationRepository.swift
//  MagixCampus
//
//  Created by User on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class NotificationRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
        networkClient = client
        cacher = Cacher(destination: .atFolder("notifications"))
    }
    func getNotificationList(completion : @escaping RepositoryCompletion) {
        guard let url = Endpoint.notificationList().url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: NotificationModel.self),
                decodingType: MagixResponse<[NotificationModel]>.self, completion: completion)
    }
}
