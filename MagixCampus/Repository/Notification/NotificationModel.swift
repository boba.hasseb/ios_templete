//
//  NotificationModel.swift
//  MagixCampus
//
//  Created by User on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct NotificationModel: Codable {
    var notificationID: String
    var name: String?
    var body: String?
    var insertDate: String?
}
