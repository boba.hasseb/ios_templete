//
//  NotificationUiModel.swift
//  MagixCampus
//
//  Created by User on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct NotificationUiModel: Codable {
    var notificatoinID: String
    var notificatoinName: String?
    var notificatoinBody: String?
    var notificatoinTimeAgo: String?
}
