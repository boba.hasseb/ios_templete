//
//  NewsListRepository.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class NewsListRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
        networkClient = client
        cacher = Cacher(destination: .atFolder("NewsList"))
    }
    func getNewsListData(completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.newsList().url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: NewsListModel.self),
                decodingType: MagixResponse<[NewsListModel]>.self, completion: completion)
    }
}
