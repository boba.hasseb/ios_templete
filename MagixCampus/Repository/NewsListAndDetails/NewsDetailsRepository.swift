//
//  NewsDetailsRepository.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class NewsDetailsRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
        networkClient = client
        cacher = Cacher(destination: .atFolder("NewsDetails"))
    }
    func getNewsDetailstData(with contentId: String, completion: @escaping
        RepositoryCompletion) {
        guard let url = Endpoint.newsDetails(contentId: contentId).url
            else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: CampusInformationDetailsModel.self),
                decodingType: MagixResponse<NewsDetailsModel>.self,
                  completion: completion)
    }
}
