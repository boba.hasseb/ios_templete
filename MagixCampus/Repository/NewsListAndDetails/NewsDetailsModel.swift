//
//  NewsDetailsModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct NewsDetailsModel: Codable {
       var contentID: String
       var name: String?
       var contentImagePath: String?
       var description: String?
}
