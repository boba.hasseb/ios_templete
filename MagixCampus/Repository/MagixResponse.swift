//
//  MagixResponse.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/8/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct MagixResponse<T: Codable>: Codable, Cachable {
    var fileName: String? = String(describing: T.self)
    var version: String?
    var statusCode: Int?
    var message: String?
    var responseException: ResponseException?
    var result: T?
}
struct ResponseException: Codable {
    var exceptionMessage: String?
}
