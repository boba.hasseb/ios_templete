//
//  CampusInformationDetails.swift
//  MagixCampus
//
//  Created by Mohamed on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct CampusInformationDetailsModel: Codable {
       var contentID: String
       var contentImagePath: String?
       var name: String?
       var description: String?
}
