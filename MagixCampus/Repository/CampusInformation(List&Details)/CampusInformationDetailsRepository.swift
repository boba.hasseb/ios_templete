//
//  CampusInformationDetailsRepository.swift
//  MagixCampus
//
//  Created by Mohamed on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class CampusInformationDetailsRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
           networkClient = client
           cacher = Cacher(destination: .atFolder("CampusInformationDetails"))
       }
    func getCampusInformationDetailstData(with contentId: String, completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.informationDetails(contentId: contentId).url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: CampusInformationDetailsModel.self),
                decodingType: MagixResponse<CampusInformationDetailsModel>.self,
                  completion: completion)
    }
}
