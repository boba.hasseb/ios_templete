//
//  CampusInformationListRepository.swift
//  MagixCampus
//
//  Created by Mohamed on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class CampusInformationListRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
           networkClient = client
           cacher = Cacher(destination: .atFolder("CampusInformationList"))
       }
    func getCampusInformationListData(completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.informationList().url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: CampusInformationListModel.self),
                decodingType: MagixResponse<[CampusInformationListModel]>.self, completion: completion)
    }
}
