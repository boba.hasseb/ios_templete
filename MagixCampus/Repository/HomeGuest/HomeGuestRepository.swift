//
//  HomeGuestRepository.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class HomeGuestRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter

    public init(_ client: APIRouter = LocalClient(fileName: "news_list_response")) {
        networkClient = client
        cacher = Cacher(destination: .atFolder("HomeGuest"))
    }
    func getVideoLink(completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.list().url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: NewsListModel.self),
                decodingType: MagixResponse<[NewsListModel]>.self, completion: completion)
    }
}
