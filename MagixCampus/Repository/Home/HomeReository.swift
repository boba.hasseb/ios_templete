//
//  HomeReository.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class HomeReository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter

    public init(_ client: APIRouter = NetworkClient()) {
        networkClient = client
        cacher = Cacher(destination: .atFolder("Home"))
    }
}
