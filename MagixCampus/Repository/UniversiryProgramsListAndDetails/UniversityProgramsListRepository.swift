//
//  UniversityProgramsListRepository.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class UniversityProgramsListRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
           networkClient = client
           cacher = Cacher(destination: .atFolder("ProgramsList"))
       }
    func getProgramsListData(completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.programList().url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: ProgramsListModel.self),
                decodingType: MagixResponse<[ProgramsListModel]>.self, completion: completion)
    }
}
