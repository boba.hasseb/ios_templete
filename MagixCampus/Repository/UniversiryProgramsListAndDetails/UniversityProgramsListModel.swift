//
//  UniversityProgramsListModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct ProgramsListModel: Codable {
       var contentID: String
       var name: String?
       var contentType: String?
       var description: String?
}
