//
//  UniversityCoursesModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct UniversityCoursesModel: Codable {
       var contentID: String
       var name: String?
       var description: String?
       var contentImagePath: String?
}
