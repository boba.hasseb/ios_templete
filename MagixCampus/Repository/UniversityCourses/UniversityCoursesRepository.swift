//
//  UniversityCoursesRepository.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class UniversityCoursesRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
           networkClient = client
           cacher = Cacher(destination: .atFolder("UniversityCourses"))
       }
    func getUniversityCoursesData(completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.coursesList().url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: UniversityCoursesModel.self),
                decodingType: MagixResponse<[UniversityCoursesModel]>.self, completion: completion)
    }
}
