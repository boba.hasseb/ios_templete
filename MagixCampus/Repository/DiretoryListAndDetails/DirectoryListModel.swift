//
//  DirectoryListModel.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct DirectoryListModel: Codable {
    var userContact: Contact?
    var userID: String
    var customID: String?
    var jobID: String?
    var firstName: String?
    var lastName: String?
    var displayName: String?
    var gender: String?
    var title: String?
    var imagepath: String?
    var bio:String?
}
