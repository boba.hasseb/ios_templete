//
//  DirectoryDetailsUiModel.swift
//  MagixCampus
//
//  Created by User on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct DirectoryDetailsUiModel: Codable {
    var directoryDetailsID: String
    var directoryDisplayName: String?
    var directoryDetailsEmail: String?
    var directoryDetailsImage: String?
    var directoryDetailsBio: String?
}
