//
//  DirectoryListUiModel.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct DirectoryListUiModel: Codable {
    var directoryListID: String
    var directoryDisplayName: String?
    var directoryBio: String?
    var directoryListImage: String?
    var directoryDetailsEmail: String?
}
