//
//  DirectoryListRepository.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
class DirectoryListRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
           networkClient = client
           cacher = Cacher(destination: .atFolder("DirectoryListModel"))
       }
    func getDirectoryList(userType: String, completion: @escaping RepositoryCompletion) {
        guard let url = Endpoint.directoryList(userType: userType).url else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: DirectoryListModel.self),
                decodingType: MagixResponse<[DirectoryListModel]>.self, completion: completion)
    }
}
