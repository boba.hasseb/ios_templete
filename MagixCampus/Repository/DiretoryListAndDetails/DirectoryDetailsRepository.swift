//
//  DirectoryDetailsRepository.swift
//  MagixCampus
//
//  Created by User on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

class DirectoryDetailsRepository: Repository {
    let cacher: Cacher
    let networkClient: APIRouter
    public init(_ client: APIRouter = NetworkClient()) {
           networkClient = client
           cacher = Cacher(destination: .atFolder("DirectoryDetailsModel"))
       }
    func getDirectoryDetails(userType: String, userID:
        String, completion :@escaping RepositoryCompletion) {
        guard let url = Endpoint.directoryDetails(userType: userType, userId: userID).url
            else { return }
        let request = makeRequest(url: url, parameters: nil, type: .get)
        getData(withRequest: request,
                name: String(describing: DirectoryDetailsModel.self),
                decodingType: MagixResponse<DirectoryDetailsModel>.self, completion:
                 completion)
    }
}
