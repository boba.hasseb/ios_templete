//
//  DirectoryDetailsModel.swift
//  MagixCampus
//
//  Created by User on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
struct DirectoryDetailsModel: Codable {
    var userContact: Contact?
    var userID: String
    var customID: String?
    var firstName: String?
    var lastName: String?
    var displayName: String?
    var gender: String?
    var title: String?
    var imagepath: String?
    var bio: String?
}
struct Contact: Codable {
    var email: String?
}
