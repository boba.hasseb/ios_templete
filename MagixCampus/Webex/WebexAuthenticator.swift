//
//  WebexAuthenticator.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/14/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import WebexSDK

class WebexAuthenticator {
    var webexSDK: Webex?
    private var receivedCall: Call?
    typealias CompletionHandler = (WebexResult) -> Void
    public init() {
        /*
         An [OAuth](https://oauth.net/2/) based authentication strategy
         is to be used to authenticate a user on Cisco Webex.
         */
        let authenticator = OAuthAuthenticator(clientId: WebexEnvirmonment.ClientId, clientSecret: WebexEnvirmonment.ClientSecret, scope: WebexEnvirmonment.Scope, redirectUri: WebexEnvirmonment.RedirectUri)
        self.webexSDK = Webex(authenticator: authenticator)
        self.webexSDK?.logger = KSLogger() //Register a console logger into SDK
    }

    func loginGuest(completion: @escaping CompletionHandler) {
        let header = [ "typ": "JWT", "alg": "HS256" ]

        let payload = [ "sub": "guest-user-7349", "name": "Mahdy", "iss": "Y2lzY29zcGFyazovL3VzL09SR0FOSVpBVElPTi85NmFiYzJhYS0zZGNjLTExZTUtYTE1Mi1mZTM0ODE5Y2RjOWE", "exp": "1511286849" ]
        if let jsonString = JSONStringEncoder().encode(header) , let jsonString2 = JSONStringEncoder().encode(payload) {
            let encodedData = jsonString + "." + jsonString2
            let authenticator = JWTAuthenticator()
            self.webexSDK = Webex(authenticator: authenticator)
            self.webexSDK?.logger = KSLogger()
            if let authenticator = self.webexSDK?.authenticator as? JWTAuthenticator {
                authenticator.authorizedWith(jwt: encodedData)
                self.webexRegisterPhone(completion: completion)
            }
        } else {

        }
    }

    func loginWebex(completion: @escaping CompletionHandler){

        /*
         Check wether webexSDK is already authorized, webexSDk saves authorization info in device key-chain
         -note: if user didn't logged out or didn't deauthorize, "self.webexSDK.authenticator" function will return true
         -note: if webexSDK is authorized, directly jump to login success process
         */
        if (self.webexSDK?.authenticator as! OAuthAuthenticator).authorized{
            self.webexRegisterPhone(completion: completion)
            return
        }
        guard let topViewController = UIApplication.getTopViewController() else { return }
        (self.webexSDK?.authenticator as! OAuthAuthenticator).authorize(parentViewController: topViewController) { [weak self] success in
            if success {
                /* Webex Login Success codes here... */
                if let strongSelf = self {
                    strongSelf.webexRegisterPhone(completion: completion)
                }
            }
            else {
                /* Webex Login Fail codes here... */
                    completion(.failure)
            }
        }
    }
    func webexRegisterPhone(completion: @escaping CompletionHandler) {
        /*
         Registers this phone to Cisco Webex cloud on behalf of the authenticated user.
         It also creates the websocket and connects to Cisco Webex cloud.
         - note: make sure register device before calling
         */
        let uuid = NSUUID().uuidString
        print(uuid)
        self.webexSDK?.phone.register() { error in
                if error != nil {
                    //register phone fail codes here...
                    print("fail to register")
                    completion(.failure)
                } else {
                    completion(.success)
                }
        }
    }
    func logout(){
        let authenticator = OAuthAuthenticator(clientId: WebexEnvirmonment.ClientId, clientSecret: WebexEnvirmonment.ClientSecret, scope: WebexEnvirmonment.Scope, redirectUri: WebexEnvirmonment.RedirectUri)
        self.webexSDK = Webex(authenticator: authenticator)
        self.webexSDK?.logger = KSLogger()
        self.webexSDK!.phone.deregister() { ret in
            // Deauthorizes the current user and clears any persistent state with regards to the current user.
            // If the *phone* is registered, it should be deregistered before calling this method.
            self.webexSDK?.authenticator.deauthorize()
            self.webexSDK = nil
        }
    }
        func webexCallBackInit() {
            if let phone = self.webexSDK?.phone {

                /*  Callback when call is incoming. */
                phone.onIncoming = { [weak self] call in
                    ///codes after receive cll here...
                        self?.receivedCall = call
                        self?.presentCallToastView(call)
                }
            }
        }
        func presentCallToastView(_ call: Call) {
            let callToastViewController = CallToastViewController.instantiate(fromAppStoryboard: .call)
                callToastViewController.modalPresentationStyle = .fullScreen
                callToastViewController.modalTransitionStyle = .coverVertical

                /// Answer/Reject button click block
                callToastViewController.answerBtnClickedBlock = {
                    self.presentVideoCallView()
                }
                callToastViewController.rejectBtnClickedBlock = {
                    self.receivedCall?.reject() { error in
                        if error != nil {
                            print("Decline error :\(error!)")
                        }
                    }
                }
                callToastViewController.incomingCall = self.receivedCall
                callToastViewController.webexSDK = self.webexSDK
            UIApplication.getTopViewController()?.present(callToastViewController, animated: true, completion: nil)
        }
        func presentVideoCallView() {

            let videoCallViewController = VideoCallViewController.instantiate(fromAppStoryboard: .call)
                videoCallViewController.currentCall = self.receivedCall
                videoCallViewController.videoCallRole = VideoCallRole.CallReceiver((self.receivedCall?.from?.email)!)
                videoCallViewController.webexSDK = self.webexSDK
            UIApplication.getTopViewController()?.navigationController?.pushViewController(videoCallViewController, animated: true)
            }
}

enum WebexResult {
    case success
    case failure
}
struct JSONStringEncoder {
    /**
     Encodes a dictionary into a JSON string.
     - parameter dictionary: Dictionary to use to encode JSON string.
     - returns: A JSON string. `nil`, when encoding failed.
     */
    func encode(_ dictionary: [String: Any]) -> String? {
        guard JSONSerialization.isValidJSONObject(dictionary) else {
            assertionFailure("Invalid json object received.")
            return nil
        }

        let jsonObject: NSMutableDictionary = NSMutableDictionary()
        let jsonData: Data

        dictionary.forEach { (arg) in
            jsonObject.setValue(arg.value, forKey: arg.key)
        }

        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        } catch {
            assertionFailure("JSON data creation failed with error: \(error).")
            return nil
        }

        guard let jsonString = String.init(data: jsonData, encoding: String.Encoding.utf8) else {
            assertionFailure("JSON string creation failed.")
            return nil
        }

        print("JSON string: \(jsonString)")
        return jsonString
    }
}
