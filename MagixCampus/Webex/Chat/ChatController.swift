/*
 MIT License
 
 Copyright (c) 2017-2019 MessageKit
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */

import UIKit
import MessageKit
import InputBarAccessoryView
import WebexSDK

/// A base class for the example controllers
class ChatController: MessagesViewController, MessagesDataSource {
    /// saparkSDK reperesent for the WebexSDK API instance
    var webexSDK: Webex?
    var email: String?
    var senderEmail: String?

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    var spaceID: String? {
        didSet {
            self.webexSDK?.messages.list(spaceId: self.spaceID!, completionHandler: { (response) in
                if let error = response.result.error {
                    print(error)
                    return
                }
                guard let messages = response.result.data else {return}
                self.insertMessages(messages.reversed())
            })
        }
    }
    var messageList: [ChatMessage] = []
    let refreshControl = UIRefreshControl()

    let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureMessageCollectionView()
        configureMessageInputBar()
        registerMessageCallBack()
        title = email
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    // MARK: - WebexSDK: register Message receive CallBack
    private func registerMessageCallBack() {
        self.webexSDK?.messages.onEvent = { event in
            switch event {
            case .messageReceived(let message):
                self.insertMessage(message)
            case .messageDeleted:
                break
            }
        }
    }

    func configureMessageCollectionView() {
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messageCellDelegate = self
        scrollsToBottomOnKeyboardBeginsEditing = true // default false
        maintainPositionOnKeyboardFrameChanged = true // default false
        messagesCollectionView.addSubview(refreshControl)
    }
    func configureMessageInputBar() {
        messageInputBar.delegate = self
        messageInputBar.inputTextView.tintColor = .primaryColor
        messageInputBar.sendButton.setTitleColor(.primaryColor, for: .normal)
        messageInputBar.sendButton.setTitleColor(
            UIColor.primaryColor.withAlphaComponent(0.3),
            for: .highlighted
        )
    }
    // MARK: - Helpers
    func insertMessage(_ message: Message) {
        let usr = getUser(name: message.personEmail!)
        messageList.append(ChatMessage(text: message.text!,
                                       user: usr,
                                       messageId: message.id!, date: Date(timeIntervalSinceNow: 0)))
        // Reload last section to update header/footer labels and insert a new one
        messagesCollectionView.performBatchUpdates({
            messagesCollectionView.insertSections([messageList.count - 1])
            if messageList.count >= 2 {
                messagesCollectionView.reloadSections([messageList.count - 2])
            }
        }, completion: { [weak self] _ in
                self?.messagesCollectionView.scrollToBottom(animated: true)
        })
    }
    func getUser(name: String) -> User {
        let sender = User(senderId: "000000", displayName: senderEmail!)
        let reciever = User(senderId: "000001", displayName: email!)
        SampleData.shared.senders = [sender, reciever]
        SampleData.shared.currentSender = sender
        switch name {
        case sender.displayName:
            return sender
        default:
            return reciever
        }
    }

    func isLastSectionVisible() -> Bool {
        guard !messageList.isEmpty else { return false }
        let lastIndexPath = IndexPath(item: 0, section: messageList.count - 1)
        return messagesCollectionView.indexPathsForVisibleItems.contains(lastIndexPath)
    }
    // MARK: - MessagesDataSource
    func currentSender() -> SenderType {
        // get sender 
        return SampleData.shared.currentSender
    }
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messageList.count
    }
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messageList[indexPath.section]
    }
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section % 3 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate),
                                      attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10),
                                                   NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        }
        return nil
    }

    func cellBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        return NSAttributedString(string: "Read",
                                  attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10),
                                               NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }

    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name,
                                  attributes:
            [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption1)])
    }

    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {

        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString,
                                  attributes:
            [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .caption2)])
    }
}

// MARK: - MessageCellDelegate

extension ChatController: MessageCellDelegate {
    func didTapAvatar(in cell: MessageCollectionViewCell) {
        print("Avatar tapped")
    }
    func didTapMessage(in cell: MessageCollectionViewCell) {
        print("Message tapped")
    }
    func didTapCellTopLabel(in cell: MessageCollectionViewCell) {
        print("Top cell label tapped")
    }
    func didTapCellBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom cell label tapped")
    }
    func didTapMessageTopLabel(in cell: MessageCollectionViewCell) {
        print("Top message label tapped")
    }
    func didTapMessageBottomLabel(in cell: MessageCollectionViewCell) {
        print("Bottom label tapped")
    }
}

// MARK: - MessageLabelDelegate

extension ChatController: MessageLabelDelegate {
    func didSelectAddress(_ addressComponents: [String: String]) {
        print("Address Selected: \(addressComponents)")
    }
    func didSelectDate(_ date: Date) {
        print("Date Selected: \(date)")
    }
    func didSelectPhoneNumber(_ phoneNumber: String) {
        print("Phone Number Selected: \(phoneNumber)")
    }
    func didSelectURL(_ url: URL) {
        print("URL Selected: \(url)")
    }
    func didSelectTransitInformation(_ transitInformation: [String: String]) {
        print("TransitInformation Selected: \(transitInformation)")
    }
    func didSelectHashtag(_ hashtag: String) {
        print("Hashtag selected: \(hashtag)")
    }
    func didSelectMention(_ mention: String) {
        print("Mention selected: \(mention)")
    }
    func didSelectCustom(_ pattern: String, match: String?) {
        print("Custom data detector patter selected: \(pattern)")
    }
}

// MARK: - MessageInputBarDelegate

extension ChatController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        // Here we can parse for which substrings were autocompleted
        let attributedText = messageInputBar.inputTextView.attributedText!
        let range = NSRange(location: 0, length: attributedText.length)
        attributedText.enumerateAttribute(.autocompleted, in: range, options: []) { (_, range, _) in
            let substring = attributedText.attributedSubstring(from: range)
            let context = substring.attribute(.autocompletedContext, at: 0, effectiveRange: nil)
            print("Autocompleted: `", substring, "` with context: ", context ?? [])
        }
        messageInputBar.inputTextView.text = String()
        messageInputBar.invalidatePlugins()
        // Send button activity animation
        messageInputBar.sendButton.startAnimating()
        messageInputBar.inputTextView.placeholder = "Sending..."
        let messageText = Message.Text.html(html: text)
        if let emailAddress = EmailAddress.fromString(email) {
            self.webexSDK?.messages.post(messageText,
                                         toPersonEmail: emailAddress,
                                         completionHandler: {[weak self] (response) in
                switch response.result {
                case .success(let message):
                    /// Send Message Call Back Code Here
                    self?.messageInputBar.sendButton.stopAnimating()
                    self?.messageInputBar.inputTextView.placeholder = "Aa"
                    self?.senderEmail = message.personEmail
                    self?.spaceID = message.spaceId
                    self?.messagesCollectionView.scrollToBottom(animated: true)
                case .failure(let error):
                    DispatchQueue.main.async {
                        print(error)
                        self?.title = "Sent Fail!"
                    }
                }
            })
        }
    }
    private func insertMessages(_ data: [Message]) {
        for message in data {
            insertMessage(message)
        }
    }
}
