//
//  ContentManager.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/10/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

public class ContentManager {
    public static var shared = ContentManager()
    public var dictionary = [String: String]()

    public func value(for key: String, in bundle: Bundle? = nil) -> String {
        let notFoundValue = "**\(key)**"
        var result: String = notFoundValue

        // Retrieve string from CMS
        result = dictionary[key] ?? notFoundValue

        // If not found in CMS, retrieve string from customLocalizable
        if result == notFoundValue {
            result = Bundle.main.localizedString(forKey: key,
                                                 value: notFoundValue,
                                                 table: "CustomLocalizable")
        }

        // If not found in localizable.strings, retrieve from localizable.strings(English)
        if result == notFoundValue,
            String(Locale.current.identifier.prefix(2)) != "en" {
            if let path = Bundle.main.path(forResource: "en", ofType: "lproj"),
                let languageBundle = Bundle(path: path) {
                result = languageBundle.localizedString(forKey: key,
                                                        value: notFoundValue,
                                                        table: nil)
            }
        }

        return result
    }
}
