//
//  String+localized.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/10/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

extension String {
    public func localized(manager: ContentManager = ContentManager.shared,
                          bundle: Bundle? = nil) -> String {
        return manager.value(for: self, in: bundle)
    }
}
