//
//  UniversityCoursesUIModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct UniversityCoursesUiModel: Codable {
       var name: String?
       var contentImagePath: String?
       var description: String?
}
