//
//  UniversityCoursesLocalized.swift
//  MagixCampus
//
//  Created by Mohamed on 4/7/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

extension UniversityCoursesViewController {
    func configureLocalized() {
        universityCoursesTitleLabel.text =
            R.string.customLocalizable.universitycourses_courses_title.key.localized()
        universityCoursesDescriprionLabel.text =
            R.string.customLocalizable.universitycourses_courses_description.key.localized()
    }
}
