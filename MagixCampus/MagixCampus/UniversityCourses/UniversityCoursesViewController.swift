//
//  UniversityCoursesViewController.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UniversityCoursesViewController: UIViewController, BaseViewController {
    // MARK: - Outlets
    @IBOutlet weak var universityCoursesTitleLabel: UILabel!
    @IBOutlet weak var universityCoursesDescriprionLabel: UILabel!
    @IBOutlet weak var universityCoursesTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    // MARK: - RxSwif Variables
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: - Variables
    let viewModel = UniversityCoursesViewModel()
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        regiserNib()
        setUPView()
        setUpTableView()
        addGestureForView()
        setUpBindings()
        viewModel.getUniversityCourses()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationController()
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    // MARK: - RegisterNib
    func regiserNib() {
        let tableViewCellNib = UINib(nibName: R.nib.universityCoursesTableViewCell.name, bundle: nil)
        universityCoursesTableView.register(tableViewCellNib, forCellReuseIdentifier:
            R.nib.universityCoursesTableViewCell.name)
    }
    func setUPView() {
       headerView.roundCornersBottoms(radius: 30)
    }
    func setUpTableView() {
        self.universityCoursesTableView.rowHeight = 213
        self.universityCoursesTableView.tableFooterView = UIView()
    }
    // MARK: - Hide Keyboard
     func addGestureForView() {
         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(gestureRecognizer:)))
         view.addGestureRecognizer(tapGesture)
     }
     @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
         view.endEditing(true)
     }
    // MARK: - Bindings
    func setUpBindings() {
        viewModel.isLoading
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isAnimating)
            .disposed(by: disposeBag)
        viewModel
            .isError
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isError)
            .disposed(by: disposeBag)
        universityCoursesTableView.rx
            .willDisplayCell
            .observeOn(MainScheduler.instance).subscribe(onNext: ({ (cell, _) in
                cell.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7,
                               initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                                cell.alpha = 1
                                cell.layer.transform = CATransform3DIdentity
                }, completion: nil)
            })).disposed(by: disposeBag)

        viewModel.isSuccess
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: universityCoursesTableView.rx.items(cellIdentifier:
                R.nib.universityCoursesTableViewCell.name,
                                                          cellType:
                UniversityCoursesTableViewCell.self)) { ( _, course, cell ) in
                    cell.itemCell = course
        }
        .disposed(by: disposeBag)
    }
   @IBAction func loginButton(_ sender: Any) {
       guard let loginController = LoginViewController.instantiateFromNib() else { return }
       self.navigationController?.pushViewController(loginController, animated: true)
       self.tabBarController?.tabBar.isHidden = true
        }
    }
extension UniversityCoursesViewController: UITextFieldDelegate {
        func textFieldDidChangeSelection(_ textField: UITextField) {
            viewModel.searchCourse(text: textField.text ?? "")
        }
}
