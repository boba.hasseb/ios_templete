//
//  UniversityCoursesTableViewCell.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class UniversityCoursesTableViewCell: UITableViewCell {
    @IBOutlet weak var courseNameLabel: UILabel!
    @IBOutlet weak var coursedescriptionLabel: UILabel!
    @IBOutlet weak var courseImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var itemCell: UniversityCoursesUiModel? {
           didSet {
             self.courseNameLabel.text = itemCell?.name
            guard let description = itemCell?.description else {
                self.coursedescriptionLabel.text = "See all 18 course guides"
                return
            }
            self.coursedescriptionLabel.text = description
             guard let url = URL(string: itemCell?.contentImagePath ?? "") else {
                 self.courseImage.image = UIImage(named: "courseImage")
                 return
             }
             self.courseImage.kf.setImage(with: url)
           }
       }
}
