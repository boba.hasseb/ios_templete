//
//  UniversityCoursesViewModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class UniversityCoursesViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<[UniversityCoursesUiModel]> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: - Variables
    let repository: UniversityCoursesRepository
    var cashedCourses = [UniversityCoursesUiModel]()
    // MARK: - init
    public init (_ repo: UniversityCoursesRepository = UniversityCoursesRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: - GetUniversityCourses
    func getUniversityCourses() {
        self.isLoading.onNext(true)
        repository.getUniversityCoursesData { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<[UniversityCoursesModel]> {
                   switch data.statusCode {
                   case 200:
                        guard let mappedNewsListResult =
                        self?.mapToUiUniversityCoursesModel(listResponse: data)
                              else {
                                return
                         }
                              self?.cashedCourses = mappedNewsListResult
                              print(self?.cashedCourses ?? [])
                              self?.isSuccess.onNext(mappedNewsListResult)
                   default:
                     let error = ErroeMessage(title: data.message,
                                    message: data.responseException?.exceptionMessage, action: nil)
                            self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
    func searchCourse(text: String) {
        var coursesMatched = [UniversityCoursesUiModel]()
        print(cashedCourses)
        cashedCourses.forEach { (course) in
            guard let name = course.name else {return}
            if name.hasPrefix(text.lowercased()) || name.hasPrefix(text.capitalized) {
                coursesMatched.append(course)
            }
        }
        print(coursesMatched)
        isSuccess.onNext(coursesMatched)
    }
    // MARK: - mapped to UiUniversityCourseModel
    func mapToUiUniversityCoursesModel(
        listResponse: MagixResponse<[UniversityCoursesModel]>) -> [UniversityCoursesUiModel] {
        let mappedList: [UniversityCoursesUiModel] =
            (listResponse.result?
            .compactMap { (list: UniversityCoursesModel) -> UniversityCoursesUiModel in
                return UniversityCoursesUiModel(name: list.name,
                                        contentImagePath: list.contentImagePath,
                                          description: list.description)
            })!
        return mappedList
    }
}
