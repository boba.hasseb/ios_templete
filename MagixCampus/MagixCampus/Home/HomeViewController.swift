//
//  HomeViewController.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift

class HomeViewController: UIViewController {
    // MARK: - Controller outlets
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var scheduleInterval: UILabel!
    @IBOutlet weak var scheduleButton: UIImageView!
    @IBOutlet weak var viewAllSchedule: UIButton!
    @IBOutlet weak var noOfSchedules: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var scheduleSubview: UIView!
    @IBOutlet weak var scheduleTime: UILabel!
    @IBOutlet weak var scheduleLocation: UILabel!
    @IBOutlet weak var scheduleDescreption: UILabel!
    @IBOutlet weak var scheduleTitle: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scheduleView: UIView!
    @IBOutlet weak var initialsButton: UIButton!

    // MARK: - RxSwif Variables
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: - Controller Variables
    let viewModel = HomeViewModel()
    var dataSource = [CellData]()
    var noOfCellsInRow = 2
    let userDefaults = UserDefaults.standard
    let webexAuthenticator = WebexAuthenticator()

    override func viewDidLoad() {
        super.viewDidLoad()
        createCallbacks()
        collectionView.registerNib(identifier: R.nib.collectionCell.name)
        collectionView.delegate = self
        collectionView.dataSource = self
        topView.roundCorners(radius: 30)
        webexAuthenticator.webexCallBackInit()

    }
    override func viewDidAppear(_ animated: Bool) {
        viewModel.configureHomeCells()
        viewModel.getSchedule()
        setLabelsText()
    }
    @IBAction func profileButton(_ sender: Any) {
        guard let destinationVC = ProfileViewController.instantiateFromNib() else {
            return
        }
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    func setLabelsText() {
        if let firstName = userDefaults.userFirstName, let lastName = userDefaults.userLastName {
            initialsButton.titleLabel?.text = "\(String(firstName.first ?? "K"))\(String(lastName.first ?? "M"))"
            nameLabel.text = firstName + lastName
        }
        let type = UserType(rawValue: userDefaults.userType ?? "student")
        if type == .student {
            descriptionLabel.text =
                R.string.customLocalizable.home_descriptionlabel.key.localized()
        }
    }
    func createCallbacks() {
        viewModel.isLoading
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isAnimating)
            .disposed(by: disposeBag)

        viewModel
            .isError
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isError)
            .disposed(by: disposeBag)
        viewModel
            .isSuccess
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {[weak self] (data) in
                self?.dataSource = data
                self?.collectionView.reloadData()
            }).disposed(by: disposeBag)
        viewModel
            .scheduleList
            .observeOn(MainScheduler
            .instance)
            .subscribe(onNext: {[weak self] (schedule) in
                guard let schedule = schedule else {
                    self?.removeScheduleView()
                    return
                }
                self?.showScheduleView()
                self?.scheduleTime.text = schedule.time
                self?.scheduleTitle.text = schedule.title
                self?.scheduleDescreption.text = schedule.description
                self?.scheduleLocation.text = schedule.location
            }).disposed(by: disposeBag)
    }
    func removeScheduleView() {
        viewAllSchedule.isHidden = true
        scheduleSubview.subviews.forEach({ $0.gone() })
        scheduleView.subviews.forEach({ $0.gone() })
        scheduleView.gone()
        topViewConstraint.constant = 150
    }
    func showScheduleView() {
        viewAllSchedule.isHidden = false
        scheduleSubview.subviews.forEach({ $0.visible() })
        scheduleView.subviews.forEach({ $0.visible() })
        scheduleView.visible()
        topViewConstraint.constant = 244
    }
}
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView
            .dequeueReusableCell(withReuseIdentifier: R.nib.collectionCell.name, for: indexPath) as? CollectionCell {
            cell.cellTitle.text = dataSource[indexPath.row].title
            cell.cellImage.image = dataSource[indexPath.row].image
            cell.action = dataSource[indexPath.row].action
            cell.addShadow()
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let currentCell = collectionView.cellForItem(at: indexPath) as? CollectionCell {
            if let action = currentCell.action {
                action()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout
            else { return CGSize(width: 150, height: 150) }

        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))

        return CGSize(width: size, height: size)
    }
}
extension UIView {
    func roundCorner(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
