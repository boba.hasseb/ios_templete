//
//  TabBarItem.swift
//  We
//
//  Created by ahmed mahdy on 11/11/19.
//  Copyright © 2019 Mahdy. All rights reserved.
//

import Foundation

import UIKit

enum RoundedTabBarItem {
    case homeItem, studentsItem, teachersItem, schedule, chat, courses,
    map, programms, homeGuest
}

extension RoundedTabBarItem {

    var title: String {
        switch self {
        case .homeItem:
            return "Home"
        case .studentsItem:
            return "Students"
        case .teachersItem:
            return "Teachers"
        case .schedule:
            return "Schedule"
        case .chat:
            return "Chat"
        case .courses:
            return "Courses"
        case .map:
            return "Map"
        case .programms:
            return "Programms"
        case .homeGuest:
            return "Home"
        }
    }

    var tag: Int {
        switch self {
        case .homeItem:
            return 1
        case .studentsItem:
            return 2
        case .teachersItem:
            return 3
        case .schedule:
            return 4
        case .chat:
            return 5
        case .courses:
            return 6
        case .map:
            return 7
        case .programms:
            return 8
        case .homeGuest:
            return 9
        }
    }

    var image: UIImage? {
        switch self {
        case .homeItem:
            return R.image.home()
        case .studentsItem:
            return R.image.studentstab()
        case .teachersItem:
            return R.image.teachers()
        case .schedule:
            return R.image.schedule()
        case .chat:
            return R.image.chat()
        case .courses:
            return R.image.courses()
        case .map:
            return R.image.map()
        case .programms:
            return R.image.programms()
        case .homeGuest:
            return R.image.home()
        }
    }

    var tabBarItem: UITabBarItem {
        let tabItem = UITabBarItem(title: title, image: image, tag: tag)
        return tabItem
    }
}
