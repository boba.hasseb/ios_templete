//
//  ScheduleUIModel.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 3/31/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct ScheduleUIModel {
    let time: String
    let title: String
    let description: String
    let location: String
}
