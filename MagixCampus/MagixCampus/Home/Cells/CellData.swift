//
//  CellData.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 3/31/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
struct CellData {
    let title: String
    let image: UIImage
    let action: (() -> Void)?
}
