//
//  CollectionCell.swift
//  CiscoKSA
//
//  Created by Ahmed Mahdy on 1/20/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    var action: (() -> Void)?
    func addShadow() {
        // adding transparent border to cell content view to make shadow appear with rounded corners
        contentView.layer.cornerRadius = 10.0
        contentView.layer.borderWidth = 2.0
        contentView.layer.borderColor = UIColor.clear.cgColor
        contentView.layer.masksToBounds = true

        configureShadow(radius: 4.0)
        layer.shadowPath = UIBezierPath(roundedRect: bounds,
                                        cornerRadius: contentView.layer.cornerRadius).cgPath
    }
}
