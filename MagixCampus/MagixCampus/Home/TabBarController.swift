//
//  TabBarController.swift
//  We
//
//  Created by ahmed mahdy on 11/11/19.
//  Copyright © 2019 Mahdy. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    var type: HomeType = .home
    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubviews()
        setupApperence()
    }

    // MARK: - Setups

    private func setupApperence() {
        tabBar.barTintColor = UIColor.white
        UITabBar.appearance().tintColor = UIColor.dullOrange
        UITabBarItem.appearance()
            .setTitleTextAttributes(
                [NSAttributedString.Key.foregroundColor: UIColor.dullOrange], for: .selected)
        UITabBarItem.appearance()
            .setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.greyishBrown], for: .normal)
    }

    private func setupSubviews() {
        createTabbarControllers()
    }

    // MARK: - Helpers

    private func createTabbarControllers() {
        switch type {
        case .home:
            creatHomeControllers()
        default:
            creatHomeGUestControllers()
        }
    }

    private func creatHomeControllers() {
        var systemTags = [RoundedTabBarItem]()
        if Configuration.shared.config?.home.chattingEnabled ?? false {
            systemTags = [RoundedTabBarItem.homeItem, .studentsItem, .teachersItem, .schedule, .chat]
        } else {
            systemTags = [RoundedTabBarItem.homeItem, .studentsItem, .teachersItem, .schedule]
        }
        let viewControllers = systemTags.compactMap { self.createController(for: $0, with: $0.tag) }

        self.viewControllers = viewControllers
    }

    private func creatHomeGUestControllers() {
        var systemTags = [RoundedTabBarItem]()
        if Configuration.shared.config?.homeGuest.chattingEnabled ?? false {
            systemTags = [.homeGuest, .courses, .map, .programms, .chat]
        } else {
            systemTags = [.homeGuest, .courses, .map, .programms]
        }
        let viewControllers = systemTags.compactMap { self.createController(for: $0, with: $0.tag) }

        self.viewControllers = viewControllers
    }
    private func createController(for customTabBarItem: RoundedTabBarItem, with tag: Int) -> UINavigationController? {
        let viewController = getController(forItem: customTabBarItem)
        viewController.tabBarItem = customTabBarItem.tabBarItem
        let nav = UINavigationController(rootViewController: viewController)
        nav.navigationBar.isHidden = true
        return nav
    }
    func getController(forItem: RoundedTabBarItem) -> UIViewController {
        var viewController = UIViewController()
        switch forItem {
        case .homeItem:
            viewController = HomeViewController.instantiate(fromAppStoryboard: .main)
        case .studentsItem:
            viewController = studentController()
        case .teachersItem:
            viewController = teacherController()
        case .schedule:
            viewController = schedulecontroller()
        case .chat:
            viewController = chatcontroller()
        case .courses:
            viewController = coursescontroller()
        case .map:
            viewController = mapcontroller()
        case .programms:
            viewController = programmscontroller()
        case .homeGuest:
            viewController = homeGuestcontroller()
        }
        return viewController
    }
}

extension TabBarController {
    func studentController() -> UIViewController {
        guard let studentDirectory = DirectoryListViewController.instantiateFromNib() else {return UIViewController() }
        studentDirectory.directoryType = .student
        return studentDirectory
    }
    func teacherController() -> UIViewController {
        guard let studentDirectory = DirectoryListViewController.instantiateFromNib() else {return UIViewController() }
        studentDirectory.directoryType = .teacher
        return studentDirectory
    }
    func homeGuestcontroller() -> UIViewController {
        guard let homeGuest = HomeGuestViewController.instantiateFromNib() else { return UIViewController() }
        return homeGuest
    }
    func schedulecontroller() -> UIViewController {
        return UIViewController()
    }
    func mapcontroller() -> UIViewController {
        guard let map = CampusMapViewController.instantiateFromNib() else { return UIViewController() }
        map.type = .homeGuest
        return map
    }
    func coursescontroller() -> UIViewController {
        guard let courses = UniversityCoursesViewController.instantiateFromNib() else { return UIViewController() }
        return courses
    }
    func chatcontroller() -> UIViewController {
        return UIViewController()
    }
    func programmscontroller() -> UIViewController {
        guard let programms = UniversityProgramsListViewController
            .instantiateFromNib() else { return UIViewController() }
        return programms
    }
}

enum HomeType {
    case home
    case homeGuest
}
