//
//  HomeViewModel.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class HomeViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<[CellData]> = PublishSubject()
    let scheduleList: PublishSubject<ScheduleUIModel?> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    let repository: HomeReository
    var userDefaults = UserDefaults.standard
    // MARK: - init
    public init (_ repo: HomeReository = HomeReository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }

    func configureHomeCells() {
        let firstCell = CellData(title: "Class Schedule", image: R.image.classschedule()!, action: goToSchedule)
        let secondCell = CellData(title: "Campus Map", image: R.image.mapIcon()!, action: goToMap)
        let thirdCell = CellData(title: "Teacher Directory", image: R.image.teachercell()!, action: goToTeacher)
        let forthCell = CellData(title: "Student Directory", image: R.image.students()!, action: goTostudent)
        let fifthCell = CellData(title: "News & \nAnnouncements", image: R.image.news()!, action: goToNews)
        let sixthCell = CellData(title: "Academy \nInformation", image: R.image.infoIcon()!, action: goToAbout)
        var result = [firstCell, secondCell, thirdCell, forthCell, fifthCell, sixthCell]
        if !(Configuration.shared.config?.home.campusInformationEnabled ?? true) {
            result.removeAll { $0.title == sixthCell.title }
        }
        if !(Configuration.shared.config?.home.campusMapEnabled ?? true) {
            result.removeAll { $0.title == secondCell.title }
        }
        if !(Configuration.shared.config?.home.campusNewsEnabled ?? true) {
            result.removeAll { $0.title == fifthCell.title }
        }
        self.isSuccess.onNext(result)
    }
    func goTostudent() {
        UIApplication.getTopViewController()?.tabBarController?.selectedIndex = 1
    }
    func goToTeacher() {
        UIApplication.getTopViewController()?.tabBarController?.selectedIndex = 2
    }
    func goToMap() {
        if let mapViewController = CampusMapViewController.instantiateFromNib() {
            UIApplication.getTopViewController()?.navigationController?
                .pushViewController(mapViewController, animated: true)
        }
    }
    func goToSchedule() {
        UIApplication.getTopViewController()?.tabBarController?.selectedIndex = 3
    }
    func goToNews() {
        // navigate to news
        if let newsViewController = NewsListViewController.instantiateFromNib() {
            UIApplication.getTopViewController()?.navigationController?
                .pushViewController(newsViewController, animated: true)
        }
    }
    func goToAbout() {
        if let aboutViewController = CampusInformationListViewController.instantiateFromNib() {
            UIApplication.getTopViewController()?.navigationController?
                .pushViewController(aboutViewController, animated: true)
        }
    }
    func getSchedule() {
        let schedule = ScheduleUIModel(time: "8:00",
                                       title: "Engineering Economy",
                                       description: "By Johan Albert",
                                       location: "Room C14, Faculty of Engineering... ")
        scheduleList.onNext(schedule)
    }
}
