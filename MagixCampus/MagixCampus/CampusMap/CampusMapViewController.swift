//
//  CampusMapViewController.swift
//  MagixCampus
//asdsdasas
//  Created by Mohamed on 3/31/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import WebKit

class CampusMapViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var mapWebView: WKWebView!
    // MARK: - Variables
    var userDefaults = UserDefaults.standard
    var type: HomeType = .home
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    override func viewWillAppear(_ animated: Bool) {
        switch type {
        case .homeGuest:
            back.gone()
        default:
            return
        }
    }
    func setUpView() {
        if let firstName = userDefaults.userFirstName, let lastName = userDefaults.userLastName {
             profileNameLabel.text = "\(String(firstName.first ?? "K"))\(String(lastName.first ?? "M"))"
         }
        if userDefaults.applicationSettings?.leafletURL != nil {
            mapWebView.load(URLRequest(url: URL(string:
                userDefaults.applicationSettings?.leafletURL ?? "")!))
        } else {
            mapWebView.load(URLRequest(url: URL(string: "https://www.google.com/")!))
        }
     }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
