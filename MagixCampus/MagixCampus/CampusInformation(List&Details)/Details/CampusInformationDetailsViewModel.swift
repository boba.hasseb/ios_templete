//
//  CampusInformationDetailsViewModel.swift
//  MagixCampus
//
//  Created by Mohamed on 3/31/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class CampusInformaionDetailsViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<CampusInformationDetailsUimodel> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    let repository: CampusInformationDetailsRepository
    // MARK: - init
    public init (_ repo: CampusInformationDetailsRepository = CampusInformationDetailsRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: - GetCampusIndormationDetails
    func getCampusInformationDetails(_ contentId: String) {
        self.isLoading.onNext(true)
        repository.getCampusInformationDetailstData(with: contentId ) { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<CampusInformationDetailsModel> {
                   switch data.statusCode {
                   case 200:
                        guard let mappedCampusInformationDetailsResult =
                        self?.mapToUiCampusInformationDetailsModel(listResponse: data)
                              else {
                                return
                         }
                        self?.isSuccess.onNext(mappedCampusInformationDetailsResult)
                   default:
                     let error = ErroeMessage(title: data.message,
                                    message: data.responseException?.exceptionMessage, action: nil)
                            self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
    // MARK: - mapped to UiCampusInformationDetailsModel
    func mapToUiCampusInformationDetailsModel(
        listResponse: MagixResponse<CampusInformationDetailsModel>) -> CampusInformationDetailsUimodel {
        return CampusInformationDetailsUimodel(name: listResponse.result?.name,
                    description: listResponse.result?.description,
                                image: listResponse.result?.contentImagePath)
    }
}
