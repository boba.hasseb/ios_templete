//
//  CampusInformationDetailsUiModel.swift
//  MagixCampus
//
//  Created by Mohamed on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct CampusInformationDetailsUimodel: Codable {
    var name: String?
    var description: String?
    var image: String?
}
