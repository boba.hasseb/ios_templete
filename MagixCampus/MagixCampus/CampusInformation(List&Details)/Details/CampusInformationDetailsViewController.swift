//
//  CampusInformationDetailsViewController.swift
//  MagixCampus
//
//  Created by Mohamed on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CampusInformationDetailsViewController: UIViewController {
  // MARK: - Outlets
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var image: UIImageView!
    // MARK: - RxSwif Variables
    private var disposeBag = DisposeBag()
    // MARK: - Variables
    let userDefaults = UserDefaults.standard
    let viewModel = CampusInformaionDetailsViewModel()
    var contentId = ""
    // MARK: - init
    override func viewDidLoad() {
        super.viewDidLoad()
        setUPTextView()
        setUpBindings()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.getCampusInformationDetails(contentId)
    }
    func setUPTextView() {
       backgroundView.roundCornersTops(radius: 40)
       if let firstName = userDefaults.userFirstName, let lastName =
          userDefaults.userLastName {
             profileNameLabel.text = "\(String(firstName.first ?? "K"))\(String(lastName.first ?? "M"))"
         }
    }
    func setUpBindings() {
         viewModel.isLoading
             .observeOn(MainScheduler.instance)
             .bind(to: self.rx.isAnimating)
             .disposed(by: disposeBag)
         viewModel
             .isError
             .observeOn(MainScheduler.instance)
             .bind(to: self.rx.isError)
             .disposed(by: disposeBag)
         viewModel
            .isSuccess.observeOn(MainScheduler.instance)
            .subscribe({ [weak self] (data) in
                guard let self = self else { return }
                self.nameLabel.text = data.element?.name
                guard let description = data.element?.description else {
                    self.descriptionTextView.text = "The indefinite article takes two forms."
                    return
                }
                self.descriptionTextView.text = description
                guard let url = URL(string: data.element?.image ?? "") else {
                    self.image.image = UIImage(named: R.image.grad20Ceremony.name)
                    return
                }
                self.image.kf.setImage(with: url)
            }).disposed(by: disposeBag)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
