//
//  CampusInformationListTableViewCell.swift
//  MagixCampus
//
//  Created by Mohamed on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class CampusInformationListTableViewCell: UITableViewCell {

    @IBOutlet weak var itemListNameLabel: UILabel!
    @IBOutlet weak var itemListView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code        
    }
    var itemCell: CampusInformationListUimodel? {
          didSet {
            self.itemListNameLabel.text = itemCell?.name
          }
      }
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 10, bottom: 8, right: 10))
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
