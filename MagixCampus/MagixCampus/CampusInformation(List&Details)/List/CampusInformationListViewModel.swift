//
//  CampusInformationListViewModel.swift
//  MagixCampus
//
//  Created by Mohamed on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class CampusInformaionListViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<[CampusInformationListUimodel]> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    let repository: CampusInformationListRepository
    // MARK: - init
    public init (_ repo: CampusInformationListRepository = CampusInformationListRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: - GetCampusIndormationList
    func getCampusInformationList() {
        self.isLoading.onNext(true)
        repository.getCampusInformationListData { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<[CampusInformationListModel]> {
                   switch data.statusCode {
                   case 200:
                        guard let mappedCampusInformationListResult =
                        self?.mapToUiCampusInformationListModel(listResponse: data)
                              else {
                                return
                         }
                              self?.isSuccess.onNext(mappedCampusInformationListResult)
                   default:
                     let error = ErroeMessage(title: data.message,
                                    message: data.responseException?.exceptionMessage, action: nil)
                            self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
    // MARK: - mapped to UiCampusInformationListModel
    func mapToUiCampusInformationListModel(
        listResponse: MagixResponse<[CampusInformationListModel]>) -> [CampusInformationListUimodel] {
        let mappedList: [CampusInformationListUimodel] =
            (listResponse.result?
            .compactMap { (list: CampusInformationListModel) -> CampusInformationListUimodel in
                return CampusInformationListUimodel(contentID: list.contentID, name: list.name)
            })!
        return mappedList
    }
}
