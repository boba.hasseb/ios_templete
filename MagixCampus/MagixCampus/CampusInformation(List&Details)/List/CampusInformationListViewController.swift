//
//  CampusInformationListViewController.swift
//  MagixCampus
//
//  Created by Mohamed on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CampusInformationListViewController: UIViewController, BaseViewController {
    // MARK: - Outlets
    @IBOutlet weak var campusListTitleLabel: UILabel!
    @IBOutlet weak var campusListDescriprionLabel: UILabel!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var campusListTableView: UITableView!
    @IBOutlet weak var campusListView: UIView!
    // MARK: - RxSwif Variables
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: - Variables
    let viewModel = CampusInformaionListViewModel()
    var userDefaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        regiserNib()
        setUPView()
        setUpTableView()
        setUpBindings()
        bindTableViewSelected()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.getCampusInformationList()
    }
    // MARK: - RegisterNib
    func regiserNib() {
        let nib = UINib(nibName: R.nib.campusInformationListTableViewCell.name, bundle: nil)
        campusListTableView.register(nib, forCellReuseIdentifier: R.nib.campusInformationListTableViewCell.name)
    }
    func setUPView() {
       campusListView.roundCornersBottoms(radius: 30)
        if let firstName = userDefaults.userFirstName, let lastName = userDefaults.userLastName {
            profileNameLabel.text = "\(String(firstName.first ?? "K"))\(String(lastName.first ?? "M"))"
        }
    }
    func setUpTableView() {
        self.campusListTableView.rowHeight = 80
        self.campusListTableView.tableFooterView = UIView()
    }
    func setUpBindings() {
         viewModel.isLoading
             .observeOn(MainScheduler.instance)
             .bind(to: self.rx.isAnimating)
             .disposed(by: disposeBag)
         viewModel
             .isError
             .observeOn(MainScheduler.instance)
             .bind(to: self.rx.isError)
             .disposed(by: disposeBag)
        viewModel
                .isSuccess
                .asObservable().bind(to:
                    campusListTableView.rx.items(cellIdentifier:
                       R.nib.campusInformationListTableViewCell.name,
                                                 cellType:
                        CampusInformationListTableViewCell.self)) { ( _, item, cell ) in
                           cell.itemCell = item
               }
               .disposed(by: disposeBag)
    }
    private func bindTableViewSelected() {
        Observable
                .zip(
                    campusListTableView
                        .rx
                        .itemSelected, campusListTableView
                        .rx
                        .modelSelected(CampusInformationListUimodel.self)
                )
                .bind { [weak self] indexPath, model in
                    guard let self = self else { return }
                    if let destinationVC =
                       CampusInformationDetailsViewController.instantiateFromNib() {
                       self.campusListTableView.deselectRow(at: indexPath, animated: true)
                       destinationVC.contentId =  model.contentID
                       self.navigationController?.pushViewController(destinationVC, animated: true)
                    }}.disposed(by: disposeBag)
    }
    @IBAction func profileButton(_ sender: Any) {
        //Navigate to profile
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
