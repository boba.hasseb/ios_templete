//
//  CampusInformaionListUiModel.swift
//  MagixCampus
//
//  Created by Mohamed on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct CampusInformationListUimodel: Codable {
    var contentID: String
    var name: String?
}
