//
//  CampusInformationListLocalized.swift
//  MagixCampus
//
//  Created by Mohamed on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

extension CampusInformationListViewController {
    func configureLocalized() {
        campusListTitleLabel.text =
            R.string.customLocalizable.campusinformationlist_campuslisttitlelabel.key.localized()
        campusListDescriprionLabel.text =
            R.string.customLocalizable.campusinformationlist_campuslistdescriptionlabel.key.localized()
    }
}
