//
//  EditProfileViewController.swift
//  MagixCampus
//
//  Created by User on 4/7/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    @IBOutlet weak var updateBioStackView: UIStackView!
    @IBOutlet weak var changePasswordStackView: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        updateBioStackView.addBackground(color: .white)
        changePasswordStackView.addBackground(color: .white)
    }
    @IBAction func backAction(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
}
