//
//  ProfileViewController.swift
//  MagixCampus
//
//  Created by User on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userEmailLabel: UILabel!
    @IBOutlet weak var userIdLabel: UILabel!
    @IBOutlet weak var userBioLabel: UITextView!
    @IBOutlet weak var userImageView: UIImageView!
    // MARK: - Variables
    var userDefaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPrfoileData()
    }
    func setUpPrfoileData() {
        userNameLabel.text = String(userDefaults.userFirstName ?? "") +
            String(userDefaults.userFirstName ?? "")
        userEmailLabel.text = userDefaults.userEmail
        userBioLabel.text = userDefaults.userBio
        guard let url = URL(string: userDefaults.userImagePath ?? "") else {
            self.userImageView.image = UIImage(named: R.image.attendance_img.name)
                       return
                   }
            self.userImageView.kf.setImage(with: url)
    }
    @IBAction func editProfileButton(_ sender: Any) {
        guard let destinationVC = EditProfileViewController.instantiateFromNib() else {
            return
        }
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
