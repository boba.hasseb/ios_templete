//
//  BaseViewModel.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/2/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

protocol BaseViewModel {
    var isLoading: PublishSubject<Bool> { get }
    var isError: PublishSubject<ErroeMessage> { get }
    var disposeBag: DisposeBag { get }
}
extension BaseViewModel {
    func configureDisposeBag() {
        isError.disposed(by: disposeBag)
        isLoading.disposed(by: disposeBag)
    }
}
protocol BaseViewController {
    var disposeBag: DisposeBag { get }
}
