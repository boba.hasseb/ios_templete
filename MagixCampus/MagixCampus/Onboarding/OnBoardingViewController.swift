//
//  OnBoardingViewController.swift
//  MagixCampus
//
//  Created by User on 3/25/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class OnBoardingViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    // MARK: - Variables
    let disposeBag: DisposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavigationController()
        createCallbacks()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    func createCallbacks () {
        signInButton.rx.tap
            .`do`(onNext: {
            }).subscribe(onNext: { [unowned self] in
                if let destinationVC = LoginViewController.instantiateFromNib() {
                    self.navigationController?.pushViewController(destinationVC, animated: true)
                }
            }).disposed(by: disposeBag)

        skipButton.rx.tap
            .`do`(onNext: {
            }).subscribe(onNext: {
                let homeGuest = TabBarController.instantiate(fromAppStoryboard: .main)
                homeGuest.type = .homeGuest
                UIApplication
                    .shared
                    .windows
                    .filter {$0.isKeyWindow}.first?.rootViewController = homeGuest
            }).disposed(by: disposeBag)
    }
}
