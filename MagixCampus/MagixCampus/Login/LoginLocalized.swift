//
//  LoginLocalized.swift
//  MagixCampus
//
//  Created by Mohamed on 3/25/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

extension LoginViewController {
    func configureLocalized() {
        signInLabel.text = R.string.customLocalizable.login_signinlabel.key.localized()
        signInWithUniversityLabel.text =
        R.string.customLocalizable.login_signinwithuniversityaccountlabel.key.localized()
        emailAddessLabel.text = R.string.customLocalizable.login_emailaddresslabel.key.localized()
        emailTextField.placeholder =
            R.string.customLocalizable.login_emailaddressfield_placeholder.key.localized()
        passwordTextField.placeholder =
            R.string.customLocalizable.login_passwordfield_placeholder.key.localized()
        passwordLabel.text = R.string.customLocalizable.login_passwordlabel.key.localized()
        signInButton.titleLabel?.text =
            R.string.customLocalizable.login_signinbutton.key.localized()
        forgetPasswordButton.titleLabel?.text =
            R.string.customLocalizable.login_forgetpasswordbutton.key.localized()
    }
}
