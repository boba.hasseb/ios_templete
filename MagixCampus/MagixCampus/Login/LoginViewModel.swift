//
//  LoginViewModel.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/3/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class LoginViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<Bool> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    let repository: LoginRepository
    var userDefaults = UserDefaults.standard
    // MARK: - init
    public init (_ repo: LoginRepository = LoginRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: - Validation
    func validateEntries( email: String?, password: String?) -> Bool {
        if !(email?.isValidEmail() ?? false) {
            showError(message: R.string.customLocalizable.login_email_errordescription
                .key.localized(), title:
                R.string.customLocalizable.signup_email_errortitle.key.localized())
            return false
        } else if !(password?.isValidPassword() ?? false) {
            showError(message: R.string.customLocalizable.login_password_errordescription
                .key.localized(), title:
                R.string.customLocalizable.login_password_errortitle.key.localized())
            return false
        } else {
            return true
        }
    }
    // MARK: - Error Message
    func showError(message: String, title: String) {
        let error =
            ErroeMessage(title: title,
                         message: message, action: nil)
        self.isError.onNext(error)
    }
    // MARK: - Call Login Api
    func login(with userName: String, and password: String) {
        self.isLoading.onNext(true)
        repository.login(with: userName, and: password) { [weak self] (result) in
            guard let self = self else {
                return
            }
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<LoginResultData> {
                    switch data.statusCode {
                    case 200:
                        self.saveUserData(from: data)
                        self.getApplicationSettings()
                    default:
                        self.isLoading.onNext(false)
                        let error =
                            ErroeMessage(title: data.message,
                                         message: data.responseException?.exceptionMessage, action: nil)
                        self.isError.onNext(error)
                    }
                }
            case .failure(let error):
                self.isLoading.onNext(false)
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self.isError.onNext(error)
            }
        }
    }
    func getApplicationSettings() {
        repository.getApplicationSettings { [weak self] (result) in
            guard let self = self else {
                return
            }
            self.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<ApplicationSettings> {
                    switch data.statusCode {
                    case 200:
                        self.userDefaults.applicationSettings = data.result
                        self.isSuccess.onNext(true)
                    default:
                        let error =
                            ErroeMessage(title: data.message,
                                         message: data.responseException?.exceptionMessage, action: nil)
                        self.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self.isError.onNext(error)
            }
        }
    }
    func saveUserData( from: MagixResponse<LoginResultData>) {
        userDefaults.isLoggedIn = true
        userDefaults.userID = from.result?.userID
        userDefaults.userEmail = from.result?.email
        userDefaults.userFirstName = from.result?.firstName
        userDefaults.userLastName = from.result?.lastName
        userDefaults.userImagePath = from.result?.imagePath
        userDefaults.userType = from.result?.type
        userDefaults.userBio = from.result?.bio
    }
}
