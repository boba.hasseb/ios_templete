//
//  NewsListLocalized.swift
//  MagixCampus
//
//  Created by Mohamed on 4/2/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

extension NewsListViewController {
    func configureLocalized() {
        newsListTitleLabel.text =
            R.string.customLocalizable.newslist_newslisttitlelabel.key.localized()
        newsListDescriprionLabel.text =
            R.string.customLocalizable.newslist_newslistdescriptionlabel.key.localized()
    }
}
