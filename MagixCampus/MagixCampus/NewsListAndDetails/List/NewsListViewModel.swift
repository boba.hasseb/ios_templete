//
//  NewsListViewModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class NewsListViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<[NewsListUiModel]> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    let repository: NewsListRepository
    // MARK: - init
    public init (_ repo: NewsListRepository = NewsListRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: - GetNewsList
    func getNewsList() {
        self.isLoading.onNext(true)
        repository.getNewsListData { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<[NewsListModel]> {
                   switch data.statusCode {
                   case 200:
                        let mappedNewsListResult =
                        mapToUiNewsListModel(listResponse: data)
                              self?.isSuccess.onNext(mappedNewsListResult)
                   default:
                     let error = ErroeMessage(title: data.message,
                                    message: data.responseException?.exceptionMessage, action: nil)
                            self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
}
// MARK: - mapped to UiNewsListModel
func mapToUiNewsListModel(
    listResponse: MagixResponse<[NewsListModel]>) -> [NewsListUiModel] {
    let mappedList: [NewsListUiModel] =
        (listResponse.result?
            .compactMap { (list: NewsListModel) -> NewsListUiModel in
                return NewsListUiModel(contentID: list.contentID,
                                       name: list.name,
                                       contentImagePath: list.contentImagePath,
                                       description: list.description)
            })!
    return mappedList
}
