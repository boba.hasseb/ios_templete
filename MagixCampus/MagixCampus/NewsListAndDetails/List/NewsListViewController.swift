//
//  NewsListViewController.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NewsListViewController: UIViewController, BaseViewController {
    // MARK: - Outlets
    @IBOutlet weak var newsListTitleLabel: UILabel!
    @IBOutlet weak var newsListDescriprionLabel: UILabel!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var newsListCollectionView: UICollectionView!
    @IBOutlet weak var newsListTableView: UITableView!
    @IBOutlet weak var newsListView: UIView!
    @IBOutlet weak var firstSliderView: UIView!
    @IBOutlet weak var secondSliderView: UIView!
    @IBOutlet weak var thirdSliderView: UIView!
    @IBOutlet weak var fourthSliderView: UIView!
    @IBOutlet weak var fifthSliderView: UIView!
    @IBOutlet weak var sliderStackView: UIStackView!
    // MARK: - RxSwif Variables
    let disposeBag: DisposeBag = DisposeBag()
    let isSuccessCollectionView = PublishSubject<[NewsListUiModel]>()
    let isSuccessTableView = PublishSubject<[NewsListUiModel]>()

    // MARK: - Variables
    let viewModel = NewsListViewModel()
    var views = [UIView]()
    var userDefaults = UserDefaults.standard
    // MARK: - init
    override func viewDidLoad() {
        super.viewDidLoad()
        regiserNib()
        setUPView()
        appendViews()
        setUpCollectionView()
        setUpTableView()
        setUpBindings()
        bindTableViewAndCollectionViewSelected()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.viewModel.getNewsList()
    }
    // MARK: - RegisterNib
    func regiserNib() {
        let tableViewCellNib = UINib(nibName: R.nib.newsListTableViewCell.name, bundle: nil)
        newsListTableView.register(tableViewCellNib, forCellReuseIdentifier:
            R.nib.newsListTableViewCell.name)
        let collectionViewCellNib = UINib(nibName:
            R.nib.newsListCollectionViewCell.name, bundle: nil)
        newsListCollectionView.register(collectionViewCellNib,
                                    forCellWithReuseIdentifier:
                      R.nib.newsListCollectionViewCell.name)
    }
    func setUPView() {
       newsListView.roundCornersBottoms(radius: 30)
        if let firstName = userDefaults.userFirstName, let lastName = userDefaults.userLastName {
            profileNameLabel.text = "\(String(firstName.first ?? "K"))\(String(lastName.first ?? "M"))"
        }
    }
    func appendViews() {
        views.append(firstSliderView)
        views.append(secondSliderView)
        views.append(thirdSliderView)
        views.append(fourthSliderView)
        views.append(fifthSliderView)
    }
    func setUpCollectionView() {
        newsListCollectionView.rx.setDelegate(self).disposed(by: disposeBag)
        self.newsListCollectionView.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    func setUpTableView() {
        self.newsListTableView.rowHeight = 135
        self.newsListTableView.tableFooterView = UIView()
    }
    // MARK: - Bindings
    func setUpBindings() {
        viewModel.isLoading
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isAnimating)
            .disposed(by: disposeBag)
        viewModel
            .isError
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isError)
            .disposed(by: disposeBag)

        viewModel
            .isSuccess.observeOn(MainScheduler.instance)
            .subscribe({ [weak self] (data) in
                guard let self = self else { return }
                guard let items = data.element else { return }
                if data.element?.count ?? 0 < 5 {
                    self.updateTableViewConstrain()
                    self.isSuccessTableView.onNext(items)
                } else {
                    self.showViews()
                    let prefixFiveItems = Array(items.prefix(5))
                    self.isSuccessCollectionView.onNext(prefixFiveItems)
                    let remindItems = Array(items.suffix(from: 6))
                    self.isSuccessTableView.onNext(remindItems)
                }
            }).disposed(by: disposeBag)

        self.isSuccessCollectionView
            .observeOn(MainScheduler.instance)
            .bind(to: newsListCollectionView
                .rx
                .items(cellIdentifier: R.nib.newsListCollectionViewCell.name,
                       cellType: NewsListCollectionViewCell.self)) {  (_, item, cell) in
                    cell.itemCell = item
            }.disposed(by: disposeBag)

        self.isSuccessTableView
             .observeOn(MainScheduler.instance)
             .bind(to: newsListTableView
                 .rx
                 .items(cellIdentifier: R.nib.newsListTableViewCell.name,
                        cellType: NewsListTableViewCell.self)) {  (_, item, cell) in
                     cell.itemCell = item
             }.disposed(by: disposeBag)
    }
    private func bindTableViewAndCollectionViewSelected() {
         Observable
                 .zip(
                     newsListTableView
                         .rx
                         .itemSelected, newsListTableView
                         .rx
                         .modelSelected(NewsListUiModel.self)
                 )
                 .bind { [weak self] indexPath, model in
                     guard let self = self else { return }
                     if let destinationVC =
                        NewsDetailsViewController.instantiateFromNib() {
                        self.newsListTableView.deselectRow(at: indexPath, animated: true)
                        destinationVC.contentId =  model.contentID
                        self.navigationController?.pushViewController(destinationVC, animated: true)
                     }}.disposed(by: disposeBag)

        Observable
                .zip(
                    newsListCollectionView
                        .rx
                        .modelSelected(NewsListUiModel.self), newsListCollectionView
                        .rx
                        .modelSelected(NewsListUiModel.self)
                )
                .bind { [weak self] _, model in
                    guard let self = self else { return }
                    if let destinationVC =
                       NewsDetailsViewController.instantiateFromNib() {
                       destinationVC.contentId =  model.contentID
                       self.navigationController?.pushViewController(destinationVC, animated: true)
                    }}.disposed(by: disposeBag)
     }
    func updateTableViewConstrain() {
        self.newsListTableView.topAnchor.constraint(equalTo:
            self.newsListView.bottomAnchor, constant: 20).isActive = true
    }
    func showViews() {
        self.fifthSliderView.isHidden = false
        self.fourthSliderView.isHidden = false
        self.thirdSliderView.isHidden = false
        self.secondSliderView.isHidden = false
        self.firstSliderView.isHidden = false
    }
    @IBAction func profileButton(_ sender: Any) {
         //Navigate to profile
     }
     @IBAction func backAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
     }
}
extension NewsListViewController: UICollectionViewDelegateFlowLayout,
    UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: newsListCollectionView.frame.width,
                  height: newsListCollectionView.frame.height)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 0
    }
    func collectionView(_ collectionView: UICollectionView,
                        didEndDisplaying cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if let indexPath = newsListCollectionView.indexPathsForVisibleItems.first {
               for item in views {
                   if item .tag == indexPath.row {
                       item.backgroundColor = UIColor.white.withAlphaComponent(1)
                   } else {
                       item.backgroundColor = UIColor.white.withAlphaComponent(0.3)
                   }
               }
           }
        }
}
