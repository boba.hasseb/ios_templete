//
//  NewsDetailsUiModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

struct NewsDetailsUimodel: Codable {
     var contentImagePath: String?
     var name: String?
     var description: String?
}
