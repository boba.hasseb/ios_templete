//
//  NewsDetailsViewModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class NewsDetailsViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<NewsDetailsUimodel> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    let repository: NewsDetailsRepository
    // MARK: - init
    public init (_ repo: NewsDetailsRepository = NewsDetailsRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: - GetNewsDetails
    func getNewsDetails(_ contentId: String) {
        self.isLoading.onNext(true)
        repository.getNewsDetailstData(with: contentId ) { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<NewsDetailsModel> {
                   switch data.statusCode {
                   case 200:
                        guard let mappedNewsDetailsResult =
                        self?.mapToUiNewsDetailsModel(listResponse: data)
                              else {
                                return
                         }
                        self?.isSuccess.onNext(mappedNewsDetailsResult)
                   default:
                     let error = ErroeMessage(title: data.message,
                                    message: data.responseException?.exceptionMessage, action: nil)
                            self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
    // MARK: - mapped to UiNewsDetailsModel
    func mapToUiNewsDetailsModel(
        listResponse: MagixResponse<NewsDetailsModel>) -> NewsDetailsUimodel {
        return NewsDetailsUimodel(contentImagePath:
            listResponse.result?.contentImagePath,
                                  name: listResponse.result?.name, description:
                                                listResponse.result?.description)
    }
}
