//
//  NewsListCollectionViewCell.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class NewsListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var itemCell: NewsListUiModel? {
           didSet {
             self.titleLabel.text = itemCell?.name
           // self.dateLabel.text =  itemCell?.date
            guard let imagePath = URL(string: itemCell?.contentImagePath ?? "") else {
                self.newsImage.image = UIImage(named: R.image.grad20Ceremony.name)
                return
            }
            self.newsImage.kf.setImage(with: imagePath)
            //self.newsImage
           }
       }
}
