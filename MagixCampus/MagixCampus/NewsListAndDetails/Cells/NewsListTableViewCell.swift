//
//  NewsListTableViewCell.swift
//  MagixCampus
//
//  Created by Mohamed on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class NewsListTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var newsImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    var itemCell: NewsListUiModel? {
           didSet {
            self.titleLabel.text = itemCell?.name
           // self.dateLabel.text =  itemCell?.date
           // self.writerLabel.text = itemCell?.writerLabel
            //self.newsImage = itemCell.contentImagePath
           }
       }
}
