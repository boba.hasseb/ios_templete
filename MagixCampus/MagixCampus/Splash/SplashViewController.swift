//
//  SplashViewController.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift

class SplashViewController: UIViewController, BaseViewController {
    // MARK: - Variables
    let disposeBag = DisposeBag()
    let viewModel = SplashViewModel()
    var destinationVC: UIViewController = UIViewController()
    var userDefaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel
            .isSuccess
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: {  (_) in
//                self.configureSplash()
            }).disposed(by: disposeBag)
    }

    override func viewDidAppear(_ animated: Bool) {
        viewModel.getConfig()
        let action: (Timer) -> Void = { [weak self] timer in
            guard self != nil else {
                return
            }
            self?.configureSplash()
        }
        Timer.scheduledTimer(withTimeInterval: 0,
                             repeats: false, block: action)
    }

    func configureSplash() {
        if userDefaults.isLoggedIn {
            if Configuration.shared.config?.splash.authorizedDirection == "home" {
                destinationVC =
                    TabBarController.instantiate(fromAppStoryboard: .main)
            }
        } else {
            if Configuration.shared.config?.splash.notAuthorizedDirection == "onBoarding" {
                destinationVC = OnBoardingViewController.instantiate(fromAppStoryboard: .main)
                destinationVC = UINavigationController(rootViewController: destinationVC)
            }
        }
        navigate()
    }

    func navigate() {
        UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = destinationVC
    }
}
