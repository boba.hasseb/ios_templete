//
//  SplashViewModel.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class SplashViewModel: BaseViewModel {
    // MARK: - Observers
    let isSuccess: PublishSubject<Bool> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    // MARK: - Variables
    let disposeBag: DisposeBag = DisposeBag()
    let repository: SplashRepository

    public init (_ repo: SplashRepository = SplashRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    func getConfig() {
        repository.getConfig { (result) in
            switch result {
            case .success(let data as AppConfigModel):
                Configuration.shared.config = data.result
                self.isSuccess.onNext(true)
            default:
                let error = ErroeMessage(title: "Error", message: "Internal error", action: nil)
                self.isError.onNext(error)
            }
        }
    }
}
