//
//  HomeUIModel.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

enum ContentType {
    case staticSection
    case videoSection
    case newsSection
}
struct HomeUIItem {
    let description, title: String
    let image: UIImage
    let action: (() -> Void)?
}
struct VideoItem {
    let request: URLRequest
}
