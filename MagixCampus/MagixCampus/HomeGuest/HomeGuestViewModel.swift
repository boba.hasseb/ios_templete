//
//  HomeGuestViewModel.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class HomeGuestViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<[[ContentType: [Any]]]> = PublishSubject()
    let scheduleList: PublishSubject<ScheduleUIModel?> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    let newsRepository: NewsListRepository
    let homeRepository: HomeGuestRepository
    var userDefaults = UserDefaults.standard
    var homeItemsArray = [[ContentType: [Any]]]()
    // MARK: - init
    public init (_ newsRepo: NewsListRepository = NewsListRepository(),
                 _ homeRepo: HomeGuestRepository = HomeGuestRepository()) {
        newsRepository = newsRepo
        homeRepository = homeRepo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    func createHomeCells() {
        let courses = HomeUIItem(description: "Choose from over 200 qualifications and over 400 modules.",
                                 title: "Browse our courses", image: R.image.book()!, action: goToCourses)
        let career =
            HomeUIItem(description: "Our alumni say that study with us helped them achieve their career goals.",
                       title: "Boost your career", image: R.image.book()!, action: nil)
        let programms = HomeUIItem(description: "We’re renowned for our postgraduate programmers.",
                                   title: "University programs", image: R.image.book()!, action: goToProgramms)
        let staticItems = [ContentType.staticSection: [courses, career, programms]]

        homeItemsArray.append(staticItems)
        getVideoLink()
    }
    func goToCourses() {
        UIApplication.getTopViewController()?.tabBarController?.selectedIndex = 1
    }
    func goToProgramms() {
        UIApplication.getTopViewController()?.tabBarController?.selectedIndex = 3
    }
    // MARK: - GetVideoLink
    func getVideoLink() {
        self.isLoading.onNext(true)
        homeRepository.getVideoLink { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success:
            // bind to video url
            let url = URL(string: "https://www.youtube.com/embed/PMQauUpKmaI")
            let request = URLRequest(url: url!)
            self?.homeItemsArray.append([ContentType.videoSection: [VideoItem(request: request)]])
            self?.getNewsList()
            case .failure:
            // bind to video
            let url = URL(string: "https://www.youtube.com/embed/PMQauUpKmaI")
            let request = URLRequest(url: url!)
            self?.homeItemsArray.append([ContentType.videoSection: [VideoItem(request: request)]])
            self?.getNewsList()
            }
        }
    }
    // MARK: - GetNewsList
    func getNewsList() {
        self.isLoading.onNext(true)
        newsRepository.getNewsListData { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<[NewsListModel]> {
                    switch data.statusCode {
                    case 200:
                        var mappedNewsListResult =
                            mapToUiNewsListModel(listResponse: data)
                        if mappedNewsListResult.count > 4 {
                            mappedNewsListResult = Array(mappedNewsListResult[0..<4])
                        }
                        self?.homeItemsArray.append([ContentType.newsSection: mappedNewsListResult])
                        self?.isSuccess.onNext(self!.homeItemsArray)
                    default:
                        self?.isSuccess.onNext(self!.homeItemsArray)
                    }
                }
            case .failure:
                self?.isSuccess.onNext(self!.homeItemsArray)
            }
        }
    }
}
