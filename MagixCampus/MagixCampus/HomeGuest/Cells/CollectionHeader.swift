//
//  CollectionHeader.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class CollectionHeader: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
    var action: (() -> Void)?
    @IBAction func viewAll(_ sender: Any) {
        action?()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
