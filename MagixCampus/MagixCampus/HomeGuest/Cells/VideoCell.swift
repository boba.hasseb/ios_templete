//
//  VideoCell.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import WebKit

class VideoCell: UICollectionViewCell {
    @IBOutlet weak var videoView: WKWebView!

    var videoURLRequest: URLRequest? {
        didSet {
            videoView.load(videoURLRequest!)
            self.videoView.cornerRadius = 20
        }
    }
}
