//
//  NewsCollectionCell.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class NewsCollectionCell: UICollectionViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var writerLabel: UILabel!
    @IBOutlet weak var newsImage: UIImageView!

    var itemCell: NewsListUiModel? {
        didSet {
            self.titleLabel.text = itemCell?.name
            // self.dateLabel.text =  itemCell?.date
            // self.writerLabel.text = itemCell?.writerLabel
            //self.newsImage = itemCell.contentImagePath
        }
    }
}
