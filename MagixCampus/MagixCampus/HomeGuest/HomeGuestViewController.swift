//
//  HomeGuestViewController.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 4/5/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift

class HomeGuestViewController: UIViewController {
    // MARK: - Controller outlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var universityName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    // MARK: - RxSwif Variables
    let disposeBag: DisposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
    }
}
