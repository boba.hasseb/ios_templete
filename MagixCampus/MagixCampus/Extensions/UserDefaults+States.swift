//
//  UserDefaults+States.swift
//  VFGMVA10Group
//
//  Created by Tomasz Czyżak on 10/06/2019.
//  Copyright © 2019 Vodafone. All rights reserved.
//

import Foundation

extension UserDefaults {
    private struct Keys {
        static let isLoggedIn: String = "userLoggedIn"
        static let userID: String = "userID"
        static let userEmail: String = "userEmail"
        static let userToken: String = "token"
        static let userFirstName: String = "userFirstName"
        static let userLastName: String = "userLastName"
        static let userImagePath: String = "userImagePath"
        static let userBio: String = "userBio"
        static let userType: String = "userType"
        static let campusMapUrl: String = "url"
        static let FCMToken: String = "url"
        static let applicationSettings: String = "applicationSettings"
    }

    public var isLoggedIn: Bool {
        set {
            set(newValue, forKey: Keys.isLoggedIn)
        }
        get {
            return bool(forKey: Keys.isLoggedIn)
        }
    }
    public var userID: String? {
        set {
            set(newValue, forKey: Keys.userID)
        }
        get {
            guard let userID = value(forKey: Keys.userID) as? String else { return nil }
            return userID
        }
    }
    public var userEmail: String? {
        set {
            set(newValue, forKey: Keys.userEmail)
        }
        get {
            guard let userEmail = value(forKey: Keys.userEmail) as? String else { return nil }
            return userEmail
        }
    }
    public var userToken: String? {
        set {
            set(newValue, forKey: Keys.userToken)
        }
        get {
            guard let userToken = value(forKey: Keys.userToken) as? String else { return nil }
            return userToken
        }
    }
    public var userFirstName: String? {
        set {
            set(newValue, forKey: Keys.userFirstName)
        }
        get {
            guard let userFirstName = value(forKey: Keys.userFirstName) as? String else { return nil }
            return userFirstName
        }
    }
    public var userLastName: String? {
        set {
            set(newValue, forKey: Keys.userLastName)
        }
        get {
            guard let userLastName = value(forKey: Keys.userLastName) as? String else { return nil }
            return userLastName
        }
    }
    public var userImagePath: String? {
        set {
            set(newValue, forKey: Keys.userImagePath)
        }
        get {
            guard let userImagePath = value(forKey: Keys.userImagePath) as? String else { return nil }
            return userImagePath
        }
    }
    public var userType: String? {
        set {
            set(newValue, forKey: Keys.userType)
        }
        get {
            guard let userType = value(forKey: Keys.userType) as? String else { return nil }
            return userType
        }
    }
    public var FCMToken: String? {
         set {
             set(newValue, forKey: Keys.FCMToken)
         }
         get {
             guard let FCMToken = value(forKey: Keys.FCMToken) as? String else { return nil }
             return FCMToken
         }
     }
    public var userBio: String? {
           set {
               set(newValue, forKey: Keys.userBio)
           }
           get {
               guard let userBio = value(forKey: Keys.userBio) as? String else { return nil }
               return userBio
           }
       }
    public var applicationSettings: ApplicationSettings? {
        set {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(newValue) {
                let defaults = UserDefaults.standard
                defaults.set(encoded, forKey: Keys.applicationSettings)
            }
        }
        get {
            guard let savedSettings = value(forKey: Keys.applicationSettings) as? Data else { return nil }
                let decoder = JSONDecoder()
                if let loadedSettings = try? decoder.decode(ApplicationSettings.self, from: savedSettings) {
                    return loadedSettings
                }
            return nil
        }
    }
}
