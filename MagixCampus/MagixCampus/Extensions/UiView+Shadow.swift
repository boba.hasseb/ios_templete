//
//  UiView+Shadow.swift
//  MagixCampus
//
//  Created by Ahmed Mahdy on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

extension UIView {

    public func configureShadow(offset: CGSize = CGSize(width: 0.0, height: 3.0),
                                radius: CGFloat = 6.0,
                                opacity: Float = 0.1) {
        layer.cornerRadius = radius
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.masksToBounds = false
    }

}
