//
//  StackView+AddBackground.swift
//  MagixCampus
//
//  Created by User on 4/7/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import UIKit

extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        subView.cornerRadius = 10
        subView.shadowOpacity = 6
        subView.shadowOffset = CGSize(width: 0, height: 3)
        subView.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        insertSubview(subView, at: 0)
    }
}
