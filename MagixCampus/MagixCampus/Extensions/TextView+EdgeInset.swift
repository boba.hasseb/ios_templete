//
//  TextView+EdgeInset.swift
//  MagixCampus
//
//  Created by Mohamed on 3/30/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class TextView: UITextView {
  @IBInspectable var sideInset: CGFloat = 0.0 {
    didSet {
      textContainerInset = UIEdgeInsets(top: 10, left: sideInset, bottom: 0, right: sideInset)
    }
  }
}
