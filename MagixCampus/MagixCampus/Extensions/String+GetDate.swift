//
//  String+GetDate.swift
//  MagixCampus
//
//  Created by User on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

extension String {
    func getDate() -> Date? {
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
         dateFormatter.timeZone = TimeZone(abbreviation: "en_US_POSIX")
         let delimiter = "."
         let token = self.components(separatedBy: delimiter)
         let convertedDate = dateFormatter.date(from: token[0])!
         return convertedDate
     }
    func getDatee() -> Date? {
        let isoDate = "2020-04-1T10:44:00+0000"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return dateFormatter.date(from: isoDate)!
    }
}
