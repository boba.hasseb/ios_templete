//
//  UIView+RoundCorners.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import UIKit
extension UIView {
    func roundViewCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    func roundCorners(radius: CGFloat) {
          self.layer.cornerRadius = CGFloat(radius)
          self.clipsToBounds = true
          self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
      }
}
