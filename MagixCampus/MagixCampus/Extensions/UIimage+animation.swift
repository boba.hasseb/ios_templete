//
//  UIimage+animation.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/3/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

extension UIImageView {
    func rotate() {
        let rotation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 2
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}
