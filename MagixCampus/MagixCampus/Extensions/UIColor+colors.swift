//
//  UIColor+colors.swift
//  MagixConcierge
//
//  Created by Ahmed Mahdy on 3/16/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

extension UIColor {

    @nonobjc class var black10: UIColor {
        return UIColor(white: 0.0, alpha: 0.1)
    }

    @nonobjc class var dullOrange: UIColor {
        return UIColor(red: 243.0 / 255.0, green: 146.0 / 255.0, blue: 36.0 / 255.0, alpha: 1.0)
    }
    @nonobjc class var greyishBrown: UIColor {
        return UIColor(white: 85.0 / 255.0, alpha: 1.0)
    }
}
