//
//  UIImageViewExtenion.swift
//  MagixConcierge
//
//  Created by User on 3/3/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
extension UIImageView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
