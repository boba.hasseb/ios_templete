//
//  DirectoryDetailsViewModel.swift
//  MagixCampus
//
//  Created by User on 3/31/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class DirectoryDetailsViewModel: BaseViewModel {
    // MARK: viewmodel observers
    let isSuccess: PublishSubject<DirectoryDetailsUiModel> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: viewmodel variable
    let repository: DirectoryDetailsRepository
    var cashedDirectories = [DirectoryListUiModel]()

    public init (_ repo: DirectoryDetailsRepository = DirectoryDetailsRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: call category Items API
    func getDirectoryDetails(directoryType: DirectoryType, directoryID: String) {
        var userType = ""
        switch directoryType {
        case .teacher:
            userType = "teacher"
        case .student:
            userType = "student"
        }
        self.isLoading.onNext(true)
        repository.getDirectoryDetails(userType: userType, userID: directoryID) { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<DirectoryDetailsModel> {
                    switch data.statusCode {
                    case 200:
                        guard let items = self?.mapDirectoryDetailsItem(item: data) else {
                            return
                        }
                        self?.isSuccess.onNext(items)
                    default:
                        let error =
                            ErroeMessage(title: data.message,
                                         message: data.responseException?.exceptionMessage, action: nil)
                        self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
    // MARK: map from category response items  to category Ui items
    func mapDirectoryDetailsItem(item: MagixResponse<DirectoryDetailsModel>) ->
        DirectoryDetailsUiModel {
        return DirectoryDetailsUiModel(directoryDetailsID: item.result?.userID
            ?? "", directoryDisplayName: item.result?.firstName ?? "" +
              "\(item.result?.lastName ?? "")",
                directoryDetailsEmail: item.result?.userContact?.email,
                    directoryDetailsImage: item.result?.imagepath,
                        directoryDetailsBio: item.result?.bio)
    }
}
