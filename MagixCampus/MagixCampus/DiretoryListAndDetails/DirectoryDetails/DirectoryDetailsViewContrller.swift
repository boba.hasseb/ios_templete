//
//  DirectoryDetailsViewContrller.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import Kingfisher
import WebexSDK

class DirectoryDetailsViewContrller: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var directoryImageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var verticalSpacingTextView: NSLayoutConstraint!
    @IBOutlet weak var descriptionTextviewHeight: NSLayoutConstraint!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var videoCallButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var directoryNameLabel: UILabel!
    @IBOutlet weak var directoryEmail: UILabel!
    // MARK: - Observers
    let disposeBag = DisposeBag()
    // MARK: - Variables
    var directoryType: DirectoryType?
    var directoryID: String?
    let userDefaults = UserDefaults.standard
    let viewModel = DirectoryDetailsViewModel()
    let webexAuthenticatior = WebexAuthenticator()

    override func viewDidLoad() {
        super.viewDidLoad()
        setProfileName()
        setUpBindings()
        footerView.roundViewCorners(corners: [.topLeft, .topRight], radius: 40)
        setUpConfigeration()
    }
    override func viewWillAppear(_ animated: Bool) {
        viewModel.getDirectoryDetails(directoryType: directoryType ?? .student, directoryID:
            directoryID ?? "")
    }
    private func setProfileName() {
        let name = String(userDefaults.userFirstName?.first ?? "A") + String(userDefaults.userLastName?.first ?? "K")
        profileButton.setTitle(name, for: .normal)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setUpConfigeration() {
        if !(Configuration.shared.config?.directoryDetails.chattingEnabled ?? false) {
            chatButton.gone()
        }
        if !(Configuration.shared.config?.directoryDetails.videoCallEnabled ?? false) {
            videoCallButton.gone()
        }
    }
    @IBAction func notificationButton(_ sender: Any) {
    }
    @IBAction func profileButton(_ sender: Any) {
        guard let destinationVC = ProfileViewController.instantiateFromNib() else {
            return
        }
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    func setUpBindings() {
        viewModel.isLoading
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isAnimating)
            .disposed(by: disposeBag)
        viewModel
            .isError
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isError)
            .disposed(by: disposeBag)
        viewModel
            .isSuccess.observeOn(MainScheduler.instance)
            .subscribe({ [weak self] (data) in
                guard let self = self else { return }
                if let description = data.element?.directoryDetailsBio {
                    self.descriptionTextView.text = description
                } else {
                    self.descriptionTextView.text = "default description"
                }
                if let email = data.element?.directoryDetailsEmail {
                    self.directoryEmail.text = email
                } else {
                    self.directoryEmail.text = "hussein@ipmagix.com"
                }
                if let displayName = data.element?.directoryDisplayName {
                    self.directoryNameLabel.text = displayName
                } else {
                    self.directoryNameLabel.text = "Mohamed Hussein"
                }
                guard let url = URL(string: data.element?.directoryDetailsImage ?? "") else {
                    self.directoryImageView.image = UIImage(named: R.image.attendance_img.name)
                    return
                }
                self.directoryImageView.kf.setImage(with: url)

            }).disposed(by: disposeBag)
    }
    @IBAction func chatAction(_ sender: Any) {
        if Network.reachability.status == .unreachable {
            let alert = UIAlertController(title: "No Internet",
                                          message: "You need internet connection to make calls",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            viewModel.isLoading.onNext(true)
            webexAuthenticatior.loginWebex {[weak self] (result) in
                switch result {
                case .success:
                    self?.messageToEmailButtonClicked()
                case .failure:
                    self?.viewModel.isLoading.onNext(false)
                }
            }
        }
    }
    @IBAction func videoCallAction(_ sender: Any) {
        if Network.reachability.status == .unreachable {
            let alert = UIAlertController(title: "No Internet",
                                          message: "You need internet connection to make calls",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            } else {
                viewModel.isLoading.onNext(true)
                webexAuthenticatior.loginWebex {[weak self] (result) in
                    switch result {
                    case .success:
                        if let email = self?.directoryEmail.text {
                            self?.presentVideoCallView(email)
                        }
                    case .failure:
                        self?.viewModel.isLoading.onNext(false)
                    }
                }
            }
        }
    }
    // MARK: - Webex
    extension DirectoryDetailsViewContrller {

        @objc private func messageToEmailButtonClicked() {
            let email = EmailAddress.fromString(directoryEmail.text)
            webexAuthenticatior.webexSDK?.people
                .list(email: email, id: nil, completionHandler: { [weak self] (response) in
                    self?.viewModel.isLoading.onNext(false)
                    switch response.result {
                    case .success(let persons):
                        if let person = persons.first {
                            self?.messageWithPerson(person)
                        }
                    case .failure:
                        print("no person")
                    }
                })
        }

        public func messageWithPerson( _ person: Person) {
            if let textViewController = ChatViewController.instantiateFromNib() {
                textViewController.webexSDK = webexAuthenticatior.webexSDK
                textViewController.member = person
                navigationController?.pushViewController(textViewController, animated: true)
            }
        }

        func presentVideoCallView(_ remoteAddr: String) {
            let videoCallViewController = VideoCallViewController.instantiate(fromAppStoryboard: .call)
            videoCallViewController.videoCallRole = VideoCallRole.CallPoster(remoteAddr)
            videoCallViewController.webexSDK = webexAuthenticatior.webexSDK
            navigationController?.pushViewController(videoCallViewController, animated: true)
            viewModel.isLoading.onNext(false)
        }
}
