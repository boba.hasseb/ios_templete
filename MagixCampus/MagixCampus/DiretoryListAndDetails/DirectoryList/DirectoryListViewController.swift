//
//  DirectoryListViewController.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import  RxSwift
import RxCocoa

class DirectoryListViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var descreptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableViewDirectoryList: UITableView!
    @IBOutlet weak var tableViewFilter: UITableView!
    @IBOutlet weak var profileButton: UIButton!
    // MARK: - Variables
    var directoryType: DirectoryType?
    let viewModel = DirectoryListViewModel()
    let userDefaults = UserDefaults.standard
    // MARK: observers
    let alphaDataSource = PublishSubject<[String]>()
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
        addGestureForView()
        headerView.roundCorners(radius: 30)
        selectionCellBindings()
        setUpBasicBindings()
        alphaDataSource.onNext(["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
                                "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" ])
    }
    override func viewWillAppear(_ animated: Bool) {
        setTitleAndDescription()
        setProfileName()
        getDirectoryListData(directoryType: directoryType ?? .teacher)
    }
    func setTitleAndDescription() {
        switch directoryType {
        case .teacher:
            titleLabel.text = R.string.customLocalizable.directorylist_teacher_title.key.localized()
            descreptionLabel.text = R.string.customLocalizable.directorylist_teacher_descreption.key.localized()
        case .student:
            titleLabel.text = R.string.customLocalizable.directorylist_student_title.key.localized()
            descreptionLabel.text = R.string.customLocalizable.directorylist_student_descreption.key.localized()
        default:
            return
        }
    }
    func getDirectoryListData(directoryType: DirectoryType) {
        viewModel.getDirectoryList(directoryType: directoryType)
    }
    private func setProfileName() {
        let name = String(userDefaults.userFirstName?.first ?? "T") + String(userDefaults.userLastName?.first ?? "T")
        profileButton.setTitle(name, for: .normal)
    }
    private func setTableView() {
        tableViewDirectoryList.registerNib(identifier: R.nib.directoryListCell.name)
        tableViewFilter.registerNib(identifier: R.nib.alphaFilterCell.name)
        tableViewFilter.separatorStyle = .none
        tableViewDirectoryList.separatorStyle = .none
    }
    // MARK: - Hide Keyboard
     func addGestureForView() {
         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(gestureRecognizer:)))
         view.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
     }
     @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
         view.endEditing(true)
     }
    func setUpBasicBindings() {
        viewModel.isLoading
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isAnimating)
            .disposed(by: disposeBag)
        viewModel
            .isError
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isError)
            .disposed(by: disposeBag)
        tableViewDirectoryList.rx
            .willDisplayCell
            .observeOn(MainScheduler.instance).subscribe(onNext: ({ (cell, _) in
                cell.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7,
                               initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                                cell.alpha = 1
                                cell.layer.transform = CATransform3DIdentity
                }, completion: nil)
            })).disposed(by: disposeBag)

        viewModel.isSuccess
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: tableViewDirectoryList.rx.items(cellIdentifier:
                R.nib.directoryListCell.name, cellType: DirectoryListCell.self)) { ( _, directory, cell ) in
                    cell.itemCell = directory
        }
        .disposed(by: disposeBag)
        self.alphaDataSource
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: tableViewFilter.rx.items(cellIdentifier:
                R.nib.alphaFilterCell.name, cellType: AlphaFilterCell.self)) { ( _, alpha, cell ) in
                    cell.itemCell = alpha
        }
        .disposed(by: disposeBag)
    }
    private func selectionCellBindings() {
        directoryListCellSelectionBinding()
        tableViewFilter.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let self = self else {return}
                let cell = self.tableViewFilter.cellForRow(at: indexPath) as? AlphaFilterCell
                self.viewModel.filter(char: cell?.alphaLabel.text ?? "")
                cell?.alphaLabel.textColor = #colorLiteral(red: 0.9529411765, green: 0.5725490196, blue: 0.1411764706, alpha: 1)
            }).disposed(by: disposeBag)

        tableViewFilter.rx.itemDeselected
            .subscribe(onNext: { [weak self] indexPath in
                guard let self = self else {return}
                let cell = self.tableViewFilter.cellForRow(at: indexPath) as? AlphaFilterCell
                cell?.alphaLabel.textColor = #colorLiteral(red: 0.6630240679, green: 0.8028814197, blue: 0.430667907, alpha: 1)
            }).disposed(by: disposeBag)
        viewModel.row
               .asObservable()
               .observeOn(MainScheduler.instance)
                   .subscribe {[weak self] (data) in
                       guard let self = self else {return}
                       guard let row = data.element else {return}
                       let indexPath = IndexPath(row: row, section: 0)
                      self.tableViewDirectoryList.scrollToRow(at: indexPath, at: .top, animated: true)
               }
               .disposed(by: disposeBag)
    }
    func directoryListCellSelectionBinding() {
        Observable
                   .zip(
                       tableViewDirectoryList
                           .rx
                           .itemSelected, tableViewDirectoryList
                           .rx
                           .modelSelected(DirectoryListUiModel.self)
                   )
                   .bind { [weak self] indexPath, model in
                       guard let self = self else { return }
                       if let destinationVC =
                          DirectoryDetailsViewContrller.instantiateFromNib() {
                          self.tableViewDirectoryList.deselectRow(at: indexPath, animated: true)
                          destinationVC.directoryType = self.directoryType
                          destinationVC.directoryID =  model.directoryListID
                          self.navigationController?.pushViewController(destinationVC,
                              animated: true)
                       }
                       let cell = self.tableViewDirectoryList.cellForRow(at: indexPath) as?
                                 DirectoryListCell
                          cell?.emailLabel.isEnabled = false
         }.disposed(by: disposeBag)
    }
    @IBAction func notificationButton(_ sender: Any) {
    }
    @IBAction func profileButton(_ sender: Any) {
        guard let destinationVC = ProfileViewController.instantiateFromNib() else {
            return
        }
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
}
extension DirectoryListViewController: UITextFieldDelegate {
    func textFieldDidChangeSelection(_ textField: UITextField) {
        viewModel.searchDirectory(text: textField.text ?? "")
    }
}
