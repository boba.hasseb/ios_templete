//
//  DirectoryListCell.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class DirectoryListCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var directoryListImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var itemCell: DirectoryListUiModel? {
        didSet {
            if let displayName = itemCell?.directoryDisplayName {
                self.titleLabel.text = displayName
            } else {
                self.titleLabel.text = "Mohamed Hussein"
            }
            if let email = itemCell?.directoryDetailsEmail {
                self.emailLabel.text = email
            } else {
                self.emailLabel.text = "hussein@ipmagix.com"
            }
            guard let url = URL(string: itemCell?.directoryListImage ?? "") else {
                self.directoryListImage.image = UIImage(named: R.image.attendance_img1.name)
                return
            }
            self.directoryListImage.kf.setImage(with: url)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
