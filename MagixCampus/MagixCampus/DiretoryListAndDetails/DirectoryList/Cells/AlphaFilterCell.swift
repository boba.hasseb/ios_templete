//
//  AlphaFilterCell.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class AlphaFilterCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var alphaLabel: UILabel!
    var callBackOnLabelTap: (() -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    var itemCell: String? {
        didSet {
            self.alphaLabel.text = itemCell
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
