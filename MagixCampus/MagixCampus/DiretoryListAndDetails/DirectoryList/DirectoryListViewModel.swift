//
//  DirectoryListViewModel.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class DirectoryListViewModel: BaseViewModel {
    // MARK: viewmodel observers
    let isSuccess: PublishSubject<[DirectoryListUiModel]> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let row: PublishSubject<Int> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: viewmodel variable
    let repository: DirectoryListRepository
    var cashedDirectories = [DirectoryListUiModel]()
    public init (_ repo: DirectoryListRepository = DirectoryListRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    func getDirectoryList(directoryType: DirectoryType) {
        var userType = ""
        switch directoryType {
        case .teacher:
            userType = "teacher"
        case .student:
            userType = "student"
        }
        self.isLoading.onNext(true)
        repository.getDirectoryList(userType: userType) { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<[DirectoryListModel]> {
                    switch data.statusCode {
                    case 200:
                        let mappedItems = self?.mapDirectoryListItems(catItems: data)
                        self?.cashedDirectories = mappedItems ?? []
                        self?.isSuccess.onNext(mappedItems ?? [])
                    default:
                        let error =
                            ErroeMessage(title: data.message,
                                         message: data.responseException?.exceptionMessage, action: nil)
                        self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
    func searchDirectory(text: String) {
        var vms = [DirectoryListUiModel]()
        cashedDirectories.forEach { (directory) in
            guard let displayName = directory.directoryDisplayName else {return}
            if displayName.hasPrefix(text.lowercased()) ||
                displayName.hasPrefix(text.capitalized) {
                vms.append(directory)
            }
        }
        isSuccess.onNext(vms)
    }
    func filter(char: String) {
        for (index, directory) in cashedDirectories.enumerated() {
            if directory.directoryDisplayName?.hasPrefix(char) ?? false {
                row.onNext(index)
                break
            }
        }
    }
    func mapDirectoryListItems(catItems: MagixResponse<[DirectoryListModel]>) -> [DirectoryListUiModel] {
        let mappedCategories: [DirectoryListUiModel] = (catItems
            .result?
            .compactMap { (directory: DirectoryListModel) -> DirectoryListUiModel in
                return DirectoryListUiModel(directoryListID: directory.userID,
                                            directoryDisplayName:
                    (directory.firstName ?? "") +
                    " \(directory.lastName ?? "")" ,
                                               directoryBio:
                    directory.bio, directoryListImage: directory.imagepath,
                                   directoryDetailsEmail: directory.userContact?.email)})!
        return mappedCategories
    }
}
