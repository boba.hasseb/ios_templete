//
//  DirectoryType.swift
//  MagixCampus
//
//  Created by User on 3/29/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
enum DirectoryType: String {
    case student
    case teacher
}
