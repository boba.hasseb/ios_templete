//
//  UniversityProgramListLocalized.swift
//  MagixCampus
//
//  Created by Mohamed on 4/7/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation

extension UniversityProgramsListViewController {
    func configureLocalized() {
        universityProgramsTitleLabel.text =
            R.string.customLocalizable.universityprogramslist_programs_title.key.localized()
        universityProgramsDescriprionLabel.text =
            R.string.customLocalizable.universityprogramslist_program_description.key.localized()
    }
}
