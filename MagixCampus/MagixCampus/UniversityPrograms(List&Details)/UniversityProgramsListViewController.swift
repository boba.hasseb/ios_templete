//
//  UniversityProgramsListViewController.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class UniversityProgramsListViewController: UIViewController, BaseViewController {
    // MARK: - Outlets
    @IBOutlet weak var universityProgramsTitleLabel: UILabel!
    @IBOutlet weak var universityProgramsDescriprionLabel: UILabel!
    @IBOutlet weak var universityProgramsTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    // MARK: - RxSwif Variables
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: - Variables
    let viewModel = UniversityProgramsListViewModel()
    // MARK: - Init
    override func viewDidLoad() {
        super.viewDidLoad()
        regiserNib()
        setUPView()
        setUpTableView()
        addGestureForView()
        setUpBindings()
        viewModel.getUniversityPrograms()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureNavigationController()
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    // MARK: - RegisterNib
    func regiserNib() {
        let tableViewCellNib = UINib(nibName: R.nib.universityProgramsListTableViewCell.name, bundle: nil)
        universityProgramsTableView.register(tableViewCellNib, forCellReuseIdentifier:
            R.nib.universityProgramsListTableViewCell.name)
    }
    func setUPView() {
       headerView.roundCornersBottoms(radius: 30)
    }
    func setUpTableView() {
        self.universityProgramsTableView.rowHeight = 80
        self.universityProgramsTableView.tableFooterView = UIView()
    }
    // MARK: - Hide Keyboard
     func addGestureForView() {
         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.viewTapped(gestureRecognizer:)))
         view.addGestureRecognizer(tapGesture)
     }
     @objc func viewTapped(gestureRecognizer: UITapGestureRecognizer) {
         view.endEditing(true)
     }
    // MARK: - Bindings
    func setUpBindings() {
        viewModel.isLoading
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isAnimating)
            .disposed(by: disposeBag)
        viewModel
            .isError
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isError)
            .disposed(by: disposeBag)
        universityProgramsTableView.rx
            .willDisplayCell
            .observeOn(MainScheduler.instance).subscribe(onNext: ({ (cell, _) in
                cell.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7,
                               initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                                cell.alpha = 1
                                cell.layer.transform = CATransform3DIdentity
                }, completion: nil)
            })).disposed(by: disposeBag)

        viewModel.isSuccess
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: universityProgramsTableView.rx.items(cellIdentifier:
                R.nib.universityProgramsListTableViewCell.name,
                                                          cellType:
                UniversityProgramsListTableViewCell.self)) { ( _, program, cell ) in
                    cell.itemCell = program
        }
        .disposed(by: disposeBag)
    }
   @IBAction func loginButton(_ sender: Any) {
       guard let loginController = LoginViewController.instantiateFromNib() else { return }
       self.navigationController?.pushViewController(loginController, animated: true)
       self.tabBarController?.tabBar.isHidden = true
        }
    }
extension UniversityProgramsListViewController: UITextFieldDelegate {
        func textFieldDidChangeSelection(_ textField: UITextField) {
            viewModel.searchProgram(text: textField.text ?? "")
        }
}
