//
//  UniversityProgramsListViewModel.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class UniversityProgramsListViewModel: BaseViewModel {
    // MARK: - RxSwif Variables
    let isSuccess: PublishSubject<[ProgramsListUiModel]> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: - Variables
    let repository: UniversityProgramsListRepository
    var cashedPrograms = [ProgramsListUiModel]()
    // MARK: - init
    public init (_ repo: UniversityProgramsListRepository = UniversityProgramsListRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: - GetUniversityPrograms
    func getUniversityPrograms() {
        self.isLoading.onNext(true)
        repository.getProgramsListData { [weak self] (result) in
            self?.isLoading.onNext(false)
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<[ProgramsListModel]> {
                   switch data.statusCode {
                   case 200:
                        guard let mappedNewsListResult =
                        self?.mapToUiUniversityProgramsModel(listResponse: data)
                              else {
                                return
                         }
                              self?.cashedPrograms = mappedNewsListResult
                              self?.isSuccess.onNext(mappedNewsListResult)
                   default:
                     let error = ErroeMessage(title: data.message,
                                    message: data.responseException?.exceptionMessage, action: nil)
                            self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
    func searchProgram(text: String) {
        var programsMatched = [ProgramsListUiModel]()
        cashedPrograms.forEach { (program) in
            guard let programName = program.name else {return}
            if programName.hasPrefix(text.lowercased()) ||
                 programName.hasPrefix(text.capitalized) {
                  programsMatched.append(program)
            }
        }
        isSuccess.onNext(programsMatched)
    }
    // MARK: - mapped to UiUniversityProgramModel
    func mapToUiUniversityProgramsModel(
        listResponse: MagixResponse<[ProgramsListModel]>) -> [ProgramsListUiModel] {
        let mappedList: [ProgramsListUiModel] =
            (listResponse.result?
            .compactMap { (list: ProgramsListModel) -> ProgramsListUiModel in
                return ProgramsListUiModel(contentID: list.contentID,
                                       name: list.name,
                                          description: list.description)
            })!
        return mappedList
    }
}
