//
//  UniversityProgramsListTableViewCell.swift
//  MagixCampus
//
//  Created by Mohamed on 4/6/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class UniversityProgramsListTableViewCell: UITableViewCell {

    @IBOutlet weak var programNameLabel: UILabel!
    @IBOutlet weak var programDescriptionLabel: UILabel!
        override func awakeFromNib() {
            super.awakeFromNib()
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }
        var itemCell: ProgramsListUiModel? {
               didSet {
                 self.programNameLabel.text = itemCell?.name
                 self.programDescriptionLabel.text =  itemCell?.description
               }
           }
    }
