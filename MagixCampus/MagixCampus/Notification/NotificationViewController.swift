//
//  NotificationViewController.swift
//  MagixCampus
//zxcasds
//  Created by User on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class NotificationViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var noNotificationView: UIView!
    @IBOutlet weak var notificationTableView: UITableView!
    @IBOutlet weak var notificationImage: UIImageView!
    // MARK: - Variables
    let viewModel = NotificationViewModel()
    // MARK: observers
    let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.roundCorners(radius: 35)
        setUpBasicBindings()
        setUpTableVisiblityBindings()
        setTableView()
        viewModel.getNotificationData()
    }
    func setTableView() {
        notificationTableView.separatorStyle = .none
        notificationTableView.registerNib(identifier: R.nib.notificationCell.name)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setUpTableVisiblityBindings() {
        viewModel.isSuccess
            .asObservable()
            .subscribe(onNext: {[weak self] itemList in
            guard let self = self else {return}
            if itemList.count > 0 {
                self.notificationTableView.isHidden = false
                self.headerView.isHidden = false
                self.noNotificationView.isHidden = false
            } else {
                self.notificationTableView.isHidden = true
                self.headerView.isHidden = true
                self.noNotificationView.isHidden = true
            }
        })
        .disposed(by: disposeBag)
    }
    func setUpBasicBindings() {
        viewModel.isLoading
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isAnimating)
            .disposed(by: disposeBag)
        viewModel.isSuccess
                .asObservable()
                .observeOn(MainScheduler.instance)
                .bind(to: notificationTableView.rx.items(cellIdentifier:
                    R.nib.notificationCell.name, cellType: NotificationCell.self)) { ( _, notification, cell ) in
                        cell.itemCell = notification
            }
            .disposed(by: disposeBag)
        viewModel
            .isError
            .observeOn(MainScheduler.instance)
            .bind(to: self.rx.isError)
            .disposed(by: disposeBag)
        notificationTableView.rx
            .willDisplayCell
            .observeOn(MainScheduler.instance).subscribe(onNext: ({ (cell, _) in
                cell.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7,
                               initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                                cell.alpha = 1
                                cell.layer.transform = CATransform3DIdentity
                }, completion: nil)
            })).disposed(by: disposeBag)
    }
}
extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for:
            indexPath) as? NotificationCell else {return UITableViewCell()}
        return cell
    }
}
