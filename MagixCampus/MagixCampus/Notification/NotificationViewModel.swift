//
//  NotificationViewModel.swift
//  MagixCampus
//
//  Created by User on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import RxSwift

class NotificationViewModel: BaseViewModel {
    // MARK: viewmodel observers
    let isSuccess: PublishSubject<[NotificationUiModel]> = PublishSubject()
    let isLoading: PublishSubject<Bool> = PublishSubject()
    let isError: PublishSubject<ErroeMessage> = PublishSubject()
    let disposeBag: DisposeBag = DisposeBag()
    // MARK: viewmodel variable
    let repository: NotificationRepository

    public init (_ repo: NotificationRepository = NotificationRepository()) {
        repository = repo
        isSuccess.disposed(by: disposeBag)
        configureDisposeBag()
    }
    // MARK: call category Items API
    func getNotificationData() {
        repository.getNotificationList { [weak self] (result) in
            switch result {
            case .success(let data):
                if let data = data as? MagixResponse<[NotificationModel]> {
                    switch data.statusCode {
                    case 200:
                        guard let items = self?.mapResponseToNotificationListUIItems(items: data) else {
                            return
                        }
                        self?.isSuccess.onNext(items)
                    default:
                        let error =
                            ErroeMessage(title: data.message,
                                         message: data.responseException?.exceptionMessage, action: nil)
                        self?.isError.onNext(error)
                    }
                }
            case .failure(let error):
                let error = ErroeMessage(title: "Error", message: error.localizedDescription, action: nil)
                self?.isError.onNext(error)
            }
        }
    }
    func mapResponseToNotificationListUIItems(items: MagixResponse<[NotificationModel]>) -> [NotificationUiModel] {
           let mappedItems: [NotificationUiModel] = (items
               .result?
               .compactMap { (item: NotificationModel) -> NotificationUiModel in
                return NotificationUiModel(notificatoinID: item.notificationID,
                        notificatoinName: item.name, notificatoinBody: item.body,
                            notificatoinTimeAgo: item.insertDate)
            })!
           return mappedItems
       }
}
