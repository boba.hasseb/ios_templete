//
//  NotificationCell.swift
//  MagixCampus
//
//  Created by User on 4/1/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var notificationNameLabel: UILabel!
    @IBOutlet weak var notificationBodyLabel: UILabel!
    @IBOutlet weak var notificationTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let dateString = "2016-04-14T10:44:00+0000"
        let convertedDate = dateString.getDatee()
        notificationTimeLabel.text = convertedDate?.timeAgoSinceDate()
    }
    var itemCell: NotificationUiModel? {
           didSet {
            notificationNameLabel.text = itemCell?.notificatoinName
            notificationBodyLabel.text = itemCell?.notificatoinBody
           }
       }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
