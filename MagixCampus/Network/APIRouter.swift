//
//  APIClient.swift
//  We
//
//  Created by ahmed mahdy on 11/2/19.
//  Copyright © 2019 Mahdy. All rights reserved.
//

import Foundation
import Alamofire

protocol APIRouter {
    func makeRequest(withRequest: URLRequest, completion: @escaping JSONTaskCompletionHandler)
    func makeRequest<T: Cachable>(withRequest: URLRequest,
                                  decodingType: T.Type,
                                  completion: @escaping JSONTaskCompletionHandler)  where T: Codable
    // swiftlint:disable:next function_parameter_count
    func uploadRequest<T: Cachable>(toUrl: URL,
                                    with data: Data,
                                    with name: String,
                                    and image: UIImage?,
                                    decodingType: T.Type, completion: @escaping JSONTaskCompletionHandler)
        where T: Codable
}

extension APIRouter {
    typealias JSONTaskCompletionHandler = (RequestResult<Cachable, RequestError>) -> Void
    // swiftlint:disable:next function_parameter_count
    func uploadRequest<T: Cachable>(toUrl: URL,
                                    with data: Data,
                                    with name: String,
                                    and image: UIImage?,
                                    decodingType: T.Type, completion: @escaping JSONTaskCompletionHandler)
        where T: Codable {
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(data, withName: name)
            if let image = image {
                if let data = image.jpegData(compressionQuality: 0.5) {
                    multipartFormData.append(data, withName: "image", fileName: "image.jpeg", mimeType: "image/*")
                }
            }
        },
                         to: toUrl,
                         method: .post,
                         headers: nil,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(request:let upload,
                                          streamingFromDisk: _,
                                          streamFileURL:_):
                                upload.responseJSON(completionHandler: { (response: DataResponse<Any>) in
                                    switch response.result {
                                    case .failure:
                                        completion(.failure(.invalidResponse))
                                    case .success:
                                        self.decodeJsonResponse(decodingType: decodingType,
                                                                jsonObject: response.data!,
                                                                completion: completion)
                                    }
                                })
                            case .failure:
                                completion(.failure(.unknownError))
                            }
        })
    }
    func makeRequest(withRequest: URLRequest, completion: @escaping JSONTaskCompletionHandler) {
        Alamofire.request(withRequest)
            .responseJSON(completionHandler: { (response) in
                if let error = response.error {
                    print(error)
                    completion(.failure(.connectionError))
                } else {
                    print(response)
                    if let code = response.response?.statusCode {
                        let result = response.result
                        switch code {
                        case 200:
                            //completion(.success(responseJson))
                            if result.isSuccess {
                                completion(.success(nil))
                            } else {
                                completion(.failure(.unknownError))
                            }
                        case 400 ... 499:
                            completion(.failure(.authorizationError))
                        case 500 ... 599:
                            completion(.failure(.serverError))
                        default:
                            completion(.failure(.unknownError))
                        }
                    }
                }
            })
    }
    func makeRequest<T: Cachable>(withRequest: URLRequest,
                                  decodingType: T.Type,
                                  completion: @escaping JSONTaskCompletionHandler)  where T: Codable {
        Alamofire.request(withRequest)
            .responseJSON(completionHandler: { (response) in
                if let error = response.error {
                    print(error)
                    completion(.failure(.connectionError))
                } else if let data = response.data {
                    print(response)
                    if let code = response.response?.statusCode {
                        switch code {
                        case 200:
                            //                            completion(.success(responseJson))
                            self.decodeJsonResponse(
                                decodingType: decodingType,
                                jsonObject: data,
                                completion: completion)
                        case 400 ... 499:
                            completion(.failure(.authorizationError))
                        case 500 ... 599:
                            completion(.failure(.serverError))
                        default:
                            completion(.failure(.unknownError))
                        }
                    }
                }
            })
    }
    func decodeJsonResponse<T: Cachable>(decodingType: T.Type,
                                         jsonObject: Data,
                                         completion: @escaping JSONTaskCompletionHandler) where T: Codable {
            do {
                var genericModel = try JSONDecoder().decode(decodingType, from: jsonObject)
                genericModel.fileName = String(describing: T.self)
                completion(.success(genericModel))
            } catch {
                completion(.failure(.jsonConversionFailure))
            }
    }
}
func makeRequest(url: URL, parameters: [String: Any]?, type: HTTPMethod) -> URLRequest {
    var urlRequest = URLRequest(url: url, timeoutInterval: 10)
    do {
        urlRequest.httpMethod = type.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if let parameters = parameters {
            urlRequest.httpBody   = try JSONSerialization.data(withJSONObject: parameters)
        }
        return urlRequest
    } catch let error {
        print("Error : \(error.localizedDescription)")
    }
    return urlRequest
}
