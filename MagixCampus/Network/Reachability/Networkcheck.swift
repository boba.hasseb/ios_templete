//
//  Networkcheck.swift
//  CiscoKSA
//
//  Created by Ahmed Mahdy on 2/19/20.
//  Copyright © 2020 Ahmed Mahdy. All rights reserved.
//

import Foundation
import Alamofire

struct Network {
    static var reachability: CheckConnection!
    enum Status: String {
        case unreachable, wifi, wwan
    }
    enum Error: Swift.Error {
        case failedToSetCallout
        case failedToSetDispatchQueue
        case failedToCreateWith(String)
        case failedToInitializeWith(sockaddr_in)
    }
}
